#! /usr/bin/env python3

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022-2024 Red Hat, Inc.

# Show various data related to a testrun:
# keys/values, cluster membership [coming soon], testrun files, testsuite results

import argparse
import sqlite3
import logging
import os
import jinja2
import git
from collections import defaultdict
from typing import Dict

# globals
args : argparse.Namespace


jinja_templates = {
    "text": """testrun: {{ datagrid.testrun }}

metadata:
{% for k,v in datagrid.metadata|dictsort() %}{{ k|e }}:	{{ v|e }}
{% endfor %}
{%- if datagrid.dgtestsuites|length > 0 %}
dejagnu testsuites:
{% for k,v in datagrid.dgtestsuites|dictsort() -%}
{{ v.sumfile }}	{{ k }}
{%- if v.PASS %}	{{ v.PASS }} PASS{% endif %}
{%- if v.FAIL %}	{{ v.FAIL }} FAIL{% endif %}
{%- if v.XPASS %}	{{ v.XPASS }} XPASS{% endif %}
{%- if v.XFAIL %}	{{ v.XFAIL }} XFAIL{% endif %}
{%- if v.IPASS %}	{{ v.IPASS }} IPASS{% endif %}
{%- if v.IFAIL %}	{{ v.IFAIL }} IFAIL{% endif %}
{%- if v.KFAIL %}	{{ v.KFAIL }} KFAIL{% endif %}
{%- if v.KPASS %}	{{ v.KPASS }} KPASS{% endif %}
{%- if v.ERROR %}	{{ v.ERROR }} ERROR{% endif %}
{%- if v.UNKNOWN %}	{{ v.UNKNOWN }} UNKNOWN{% endif %}
{%- if v.UNTESTED %}	{{ v.UNTESTED }} UNTESTED{% endif %}
{%- if v.UNRESOLVED %}	{{ v.UNRESOLVED }} UNRESOLVED{% endif %}
{%- if v.UNSUPPORTED %}	{{ v.UNSUPPORTED }} UNSUPPORTED{% endif %}
{%- if v.PATH %}	{{ v.PATH }} PATH{% endif %}
{%- if v.WARNING %}	{{ v.WARNING }} WARNING{% endif %}
{% endfor %}
{% endif %}
{%- if datagrid.dgtestcases|length > 0 %}
dejagnu {{ datagrid.dgexpfile }} testcases:
{%- for tc in datagrid.dgtestcases %}
{{ tc.subtest }}	{{ tc.sumfile }}:{{ tc.sumcursor }}	{% if tc.logfile and tc.logcursor %}{{ tc.logfile }}:{{ tc.logcursor }}{% endif %}	{{ tc.result }}
{%- endfor %}
{%- endif %}
{%- if datagrid.amtestsuites|length > 0 %}
automake testsuites:
{% for k,v in datagrid.amtestsuites|dictsort() -%}
{{ k }}	
{%- if v.PASS %}	{{ v.PASS }} PASS{% endif %}
{%- if v.FAIL %}	{{ v.FAIL }} FAIL{% endif %}
{%- if v.SKIP %}	{{ v.SKIP }} SKIP{% endif %}
{%- if v.XFAIL %}	{{ v.XFAIL }} XFAIL{% endif %}
{%- if v.XPASS %}	{{ v.XPASS }} XPASS{% endif %}
{%- if v.ERROR %}	{{ v.ERROR }} ERROR{% endif %}
{% endfor %}
{%- endif %}
{%- if datagrid.amtestcases|length > 0 %}
automake {{ datagrid.amtestdir }} testcases:
{%- for tc in datagrid.amtestcases %}
{{ tc.trsfile }}	{{ tc.result }}
{%- endfor %}
{% endif %}
{%- if datagrid.glibctestsuites|length > 0 %}
glibc testsuites:
{% for k,v in datagrid.glibctestsuites|dictsort() -%}
{{ k }}	
{%- if v.PASS %}	{{ v.PASS }} PASS{% endif %}
{%- if v.FAIL %}	{{ v.FAIL }} FAIL{% endif %}
{%- if v.SKIP %}	{{ v.SKIP }} SKIP{% endif %}
{%- if v.XFAIL %}	{{ v.XFAIL }} XFAIL{% endif %}
{%- if v.XPASS %}	{{ v.XPASS }} XPASS{% endif %}
{%- if v.UNSUPPORTED %}	{{ v.UNSUPPORTED }} UNSUPPORTED{% endif %}
{% endfor %}
{%- endif %}
{%- if datagrid.glibctestcases|length > 0 %}
glibc {{ datagrid.glibctestdir }} testcases:
{%- for tc in datagrid.glibctestcases %}
{{ tc.trfile }}	{{ tc.result }}
{%- endfor %}
{% endif %}
{%- if datagrid.autotest_testcases|length > 0 %}
autotest testsuites:
{% for k,v in datagrid.autotest_testcases|dictsort() -%}
{{ k }}. {{ v['name'] }} {{ v['result'] }}
{% endfor %}
{% endif %}
{%- if datagrid.autotest_summary|length > 0 %}
testsuite summary:
{% for k,v in datagrid.autotest_summary|dictsort() -%}
{{ k }}\t{{ v }}
{% endfor %}
{% endif %}

{%- if datagrid.autoconflogtestsuites|length > 0 %}
autoconflog testsuites:
{% for k,v in datagrid.autoconflogtestsuites|dictsort() -%}
{{ k }}	{{ v }}
{% endfor %}
{%- endif %}
{%- if datagrid.autoconflogtestcases|length > 0 %}
autoconflog {{ datagrid.autoconflog }} testcases:
{%- for tc in datagrid.autoconflogtestcases %}
{{ tc.cursor }}	{{ tc.test }}	{{ tc.result }}
{%- endfor %}
{% endif %}
files:
{% for file in datagrid.files %}{{ file }}
{% endfor %}
{% if datagrid.file is not none %}file:
{{ datagrid.file }}
{% endif %}""",

    "testrun":"{{ datagrid.testrun }}",

    "files":"""{% for file in datagrid.files %}{{ file }}
{% endfor %}""",

    "file":"{{ datagrid.file }}",

    "json":"{{ datagrid|tojson|safe }}"
}


def main() -> None:
    parser = argparse.ArgumentParser(description='Print testrun metadata and files.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--git',type=str,help='git testrun archive',default='.')
    parser.add_argument('--db',type=str,help='sqlite database file',default='bunsen.sqlite3')
    parser.add_argument('--loglevel',type=str,help='logging level',default='info')
    parser.add_argument('--template',type=str,help='jinja template',default='text',choices=jinja_templates.keys())
    parser.add_argument('commitish',metavar='COMMITISH',type=str,help='testrun commit or tag',nargs=1)
    parser.add_argument('--file',type=str,help='include given file contents')
    parser.add_argument('--dejagnu',action='store_true',help='include dejagnu summary data',default=False)
    parser.add_argument('--dgexpfile',type=str,help='include testcases in given dejagnu expfile name')
    parser.add_argument('--automake',action='store_true',help='include automake summary data',default=False)    
    parser.add_argument('--amtestdir',type=str,help='include testcases in given automake testdir')
    parser.add_argument('--glibc',action='store_true',help='include glibc summary data',default=False)        
    parser.add_argument('--autotest',action='store_true',help='include autotest summary data',default=False)
    parser.add_argument('--autotestlog',type=str,help='include testcases in given autotest testlog')
    parser.add_argument('--glibctestdir',type=str,help='include testcases in given glibc testdir')
    parser.add_argument('--autoconf',action='store_true',help='include autoconf summary data',default=False)
    parser.add_argument('--autoconflog',type=str,help='include testcases in given autoconf config.log')
    parser.add_argument('--clusters',action='store_true',help='include cluster adjacency data',default=False)
   
    global args
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel.upper(),
                        format="%(asctime)s:"+os.path.basename(__file__)+":%(levelname)s:%(message)s")
    logging.captureWarnings(True)

    db = sqlite3.connect(args.db, uri=True)
    db.row_factory = sqlite3.Row
    logging.debug('opened sqlite3 db=%s', args.db)

    # Open the git repo
    testruns = git.Repo(args.git) #type: ignore[attr-defined]
    logging.debug('opened git repo %s', args.git)

    # Resolve the commitish to an actual testrun commit via git
    # ... and thence to the internal trid
    trid = None
    commitish = args.commitish[0] # nargs=1 forces a list
    try:
        commit = testruns.commit(commitish)
    except Exception as a:
        parser.error('Unknown commitish %s in git repo %s' % (commitish, args.git))
    try:
        trid = list(row[0] for row in db.execute("select id from testrun where gitcommit = ?", (commit.hexsha,)))[0]
    except Exception as a:
        parser.error('Unknown testrun %s in database %s, rerun pipeline?' % (commit.hexsha, args.db))
       
    logging.debug("located testrun commitish %s commit %s id %d", args.commitish, commit.hexsha, trid)

    # Extract metadata
    metadata_kv = {}
    for k,v in db.execute("select key,value from testrun_kv where tr = ? order by key", (trid,)):
        metadata_kv.update({k:v})

    # Extract cluster relationships: for each key, identify the set of testruns in the previous, same, and next cluster
    cluster_k_same = defaultdict(lambda: [])
    cluster_k_same_v = {}
    if args.clusters:
        for k,v,tr in db.execute("""
select tc.key,tc.value,tr.gitcommit
from testrun tr, testrun_cluster tc, testrun_cluster_member tcm
where tcm.cluster = tc.id and tcm.testrun = tr.id and
  exists(select * from testrun_cluster_member tcm2 where tcm2.testrun = ? and tcm2.cluster = tc.id)
""", (trid,)):
            cluster_k_same[k].append(tr)
            cluster_k_same_v[k] = v

    cluster_k_prev = defaultdict(lambda: [])
    cluster_k_prev_v = {}
    if args.clusters:
        for k,v,tr in db.execute("""
select tc.key,tc.value,tr.gitcommit
from testrun tr, testrun_cluster tc, testrun_cluster tcn, testrun_cluster_member tcm
where tcm.cluster = tc.id and tc.id = tcn.prev and tcm.testrun = tr.id and
  exists(select * from testrun_cluster_member tcm2 where tcm2.testrun = ? and tcm2.cluster = tcn.id)
""", (trid,)):
            cluster_k_prev[k].append(tr)
            cluster_k_prev_v[k] = v

    cluster_k_next = defaultdict(lambda: [])
    cluster_k_next_v = {}
    if args.clusters:
        for k,v,tr in db.execute("""
select tc.key,tc.value,tr.gitcommit
from testrun tr, testrun_cluster tc, testrun_cluster tcn, testrun_cluster_member tcm
where tcm.cluster = tc.id and tc.id = tcn.next and tcm.testrun = tr.id and
  exists(select * from testrun_cluster_member tcm2 where tcm2.testrun = ? and tcm2.cluster = tcn.id)
""", (trid,)):
            cluster_k_next[k].append(tr)
            cluster_k_next_v[k] = v
   
    logging.debug("cluster: prev %s same %s next %s", cluster_k_prev_v , cluster_k_same_v , cluster_k_next_v)
       
    # Extract testsuites
    dgtestsuites = {}
    dgtestsuites_total_size = 0
    hasdgtestsuites = False
    if args.dejagnu:
        for logfile,sumfile,expfile,result,number in db.execute("""
select ts.logfile,ts.sumfile,tces.expfile,tces.result,tces.number
from dejagnu_testsuite ts, dejagnu_testsuite_expfile_summary_v tces
where ts.tr = ? and tces.ts = ts.id
""", (trid,)):
            if expfile not in dgtestsuites:
                dgtestsuites[expfile] = {"logfile":logfile, "sumfile":sumfile}
            dgtestsuites[expfile].update({result:number})
            dgtestsuites_total_size += number
            hasdgtestsuites = True
    else:
        for result in db.execute("""select distinct 1 from dejagnu_testsuite ts where ts.tr = ?""", (trid,)):
            hasdgtestsuites = True
        
    logging.debug("dgtestsuites %s %d results %d has", hasdgtestsuites, len(dgtestsuites), dgtestsuites_total_size)

    # Extract results of named dejagnu expfile
    dgtestcases = []
    if args.dgexpfile:
        for row in db.execute("""
select ts.logfile,tcv.logcursor,ts.sumfile,tcv.sumcursor,tcv.subtest,tcv.result
from dejagnu_testsuite ts, dejagnu_testcase_v tcv
where ts.tr = ? and tcv.testsuite = ts.id and tcv.expfile = ?
""", (trid,args.dgexpfile)):
            dgtestcases.append(dict(row))
    logging.debug("dgtestcases %d", len(dgtestcases))
    
    amtestsuites: Dict[str, Dict[int, int]] = {}
    hasamtestsuites = False
    if args.automake:
        for testdir,result,number in db.execute("""
select ts.testdir,ats.result,ats.number
from automake_testsuite ts, automake_testsuite_summary_v ats
where ts.tr = ? and ats.ts = ts.id
""", (trid,)):
            if testdir not in amtestsuites:
                amtestsuites[testdir] = {}
            amtestsuites[testdir].update({result:number})
            hasamtestsuites = True
    else:
        for result in db.execute("""select distinct 1 from automake_testsuite ts where ts.tr = ?""", (trid,)):
            hasamtestsuites = True
                
    logging.debug("amtestsuites %s %d", hasamtestsuites, len(amtestsuites))
    
    # Extract results of named automake testdir
    amtestcases = []
    if args.amtestdir:
        for row in db.execute("""
select tcv.trsfile,tcv.logfile,tcv.result
from automake_testsuite ts, automake_testcase_v tcv
where ts.tr = ? and tcv.testsuite = ts.id and tcv.testdir = ?
""", (trid,args.amtestdir)):
            amtestcases.append(dict(row))
    logging.debug("amtestcases %d", len(amtestcases))
    
    glibctestsuites: Dict[str, Dict[int, int]] = {}
    hasglibctestsuites = False
    if args.glibc:
        for testdir,result,number in db.execute("""
select tss.name,ats.result,ats.number
from glibc_testsuite ts, glibc_testsuite_summary_v ats, glibc_string tss
where ts.tr = ? and ats.ts = ts.id and ts.testdir=tss.id
""", (trid,)):
            if testdir not in glibctestsuites:
                glibctestsuites[testdir] = {}
            glibctestsuites[testdir].update({result:number})
            hasglibctestsuites = True
    else:
        for result in db.execute("""select distinct 1 from glibc_testsuite ts where ts.tr = ?""", (trid,)):
            hasglibctestsuites = True
                
    logging.debug("glibctestsuites %s %d", hasglibctestsuites, len(glibctestsuites))
    
    # Extract results of named glibc testdir
    glibctestcases = []
    if args.glibctestdir is not None:
        for row in db.execute("""
select tcv.outfile,tcv.trfile,tcv.result
from glibc_testsuite ts, glibc_testcase_v tcv
where ts.tr = ? and tcv.testsuite = ts.id and tcv.testdir = ?
""", (trid,args.glibctestdir)):
            glibctestcases.append(dict(row))
    logging.debug("glibctestcases %d", len(glibctestcases))
    
    autoconflogtestsuites = {}
    hasautoconflogtestsuites = False
    autotest_testcases: Dict[int, Dict[str, str]] = {}
    hasautotesttestsuites = False
    autotest_summary: Dict[str, int] = {}
    if args.autotest or args.autotestlog:
        sql = """
select atv.testsuite, atv.tcname, tcnameid, atv.result,
atv.testlog, atv.logcursor, atv.errlog
from autotest_testcase_v atv, autotest_testsuite ats
where atv.testsuite = ats.id and ats.tr = ?
order by atv.tcnameid"""
        params = (trid,)
        if args.autotestlog:
            sql = """
select atv.testsuite, atv.tcname, tcnameid, atv.result,
atv.testlog, atv.logcursor, atv.errlog
from autotest_testcase_v atv, autotest_testsuite ats
where atv.testsuite = ats.id and ats.tr = ? and
atv.testlog = ?
order by atv.tcnameid"""
            params = (trid,args.autotestlog)
        cursor = db.execute(sql, params)
        for testsuite,tcname,tcnameid,result,testlog,logcursor,errlog in cursor:
            if tcnameid not in autotest_testcases:
                autotest_testcases[tcnameid] = {}
            autotest_testcases[tcnameid].update({'name':tcname, 'result':result,
                                             'testlog':testlog, 'logcursor':logcursor,
                                             'errlog':errlog})
            hasautotesttestsuites = True
        for result,count in db.execute("""
select sum.result as result, sum.number as number
from autotest_testsuite_summary_v sum, autotest_testsuite ts
where sum.ts = ts.id and ts.tr = ?
order by sum.number desc
""", (trid,)):
            autotest_summary[result] = count
    if args.autoconf:
        for filename,number in db.execute("""
select s.name,count(*)
from autoconflog_testsuite ts, autoconflog_testcase ats, autoconflog_string s
where ts.tr = ? and ats.testsuite = ts.id and s.id = ts.filename
group by s.name;
""", (trid,)):
            autoconflogtestsuites[filename]=number
            hasautoconflogtestsuites = True
    else:
        for result in db.execute("""select distinct 1 from autoconflog_testsuite ts where ts.tr = ?""", (trid,)):
            hasautoconflogtestsuites = True
        
    logging.debug("autoconflogtestsuites %s %d", hasautoconflogtestsuites, len(autoconflogtestsuites))
    
    # Extract results of named autoconf log file
    autoconflogtestcases = []
    if args.autoconflog is not None:
        for row in db.execute("""
select tcv.filename,tcv.cursor,tcv.test,tcv.result
from autoconflog_testsuite ts, autoconflog_testcase_v tcv
where ts.tr = ? and tcv.testsuite = ts.id and tcv.filename = ?
""", (trid,args.autoconflog)):
            autoconflogtestcases.append(dict(row))
    logging.debug("autoconflogtestcases %d", len(autoconflogtestcases))
    
    # Extract file list
    files = sorted([t.path for t in commit.tree.traverse() if t.type == 'blob'])
    logging.debug("files %d", len(files))
    
    # Extract contents of named file
    fcontents = None
    if args.file:
        fcontents_bytes = commit.tree[args.file].data_stream.read()
        try:
            fcontents = fcontents_bytes.decode('utf-8')
        except:
            fcontents = fcontents_bytes.decode('iso-8859-1')

    jinja_params = {
        "datagrid": {"testrun":commit.hexsha,
                     "metadata":metadata_kv,
                     "files":files,
                     "file":fcontents,
                     "dgtestsuites_total_size":dgtestsuites_total_size,
                     "hasdgtestsuites":hasdgtestsuites,
                     "dgtestsuites":dgtestsuites,
                     "dgexpfile":args.dgexpfile,
                     "dgtestcases":dgtestcases,
                     "hasamtestsuites":hasamtestsuites,
                     "amtestsuites":amtestsuites,
                     "amtestdir":args.amtestdir,
                     "amtestcases":amtestcases,
                     "hasglibctestsuites":hasglibctestsuites,
                     "glibctestsuites":glibctestsuites,
                     "glibctestdir":args.glibctestdir,
                     "glibctestcases":glibctestcases,
                     "hasautotesttestsuites":hasautotesttestsuites,
                     "autotest_testcases":autotest_testcases,
                     "autotest_summary":autotest_summary,
                     "hasautoconflogtestsuites":hasautoconflogtestsuites,
                     "autoconflogtestsuites":autoconflogtestsuites,
                     "autoconflog":args.autoconflog,
                     "autoconflogtestcases":autoconflogtestcases,
                     "cluster_k_same":cluster_k_same,
                     "cluster_k_same_v":cluster_k_same_v,                    
                     "cluster_k_prev":cluster_k_prev,
                     "cluster_k_prev_v":cluster_k_prev_v,                                      
                     "cluster_k_next":cluster_k_next,
                     "cluster_k_next_v":cluster_k_next_v,                                                              
                     }
    }
    logging.debug("jinja data: %s", jinja_params)
   
    env = jinja2.Environment(loader=jinja2.DictLoader(jinja_templates))
    template = env.get_template(args.template)
    try:
        print(template.render(jinja_params), end='')
    except BrokenPipeError:
        pass

if __name__ == '__main__':
    main()
