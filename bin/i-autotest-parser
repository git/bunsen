#! /usr/bin/env python3

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

# A new or old testrun commit has come into existence.  Create or
# destroy corresponding testsuite analysis rows for all files found in
# the commit.  In our case of debugedit, which uses a handcrafted
# testsuite driver, these come in pairs SUBTEST.out &
# SUBTEST.test-result, files.  We ignore the .sum files that this
# driver creates, even though they resemble dejagnu format.  Also:
# some testrun key/value tuples may be added.

import git
import argparse
import sqlite3
import subprocess
import logging
import sys
import os
import tempfile
import re
from typing import IO, Any, Dict


# https://www.gnu.org/software/autoconf/manual/autoconf-2.68/html_node/Using-Autotest.html
# "
# It is paradoxical that portable projects depend on nonportable tools to run
# their test suite. Autoconf by itself is the paragon of this problem: although
# it aims at perfectly portability, up to 2.13 its test suite was using DejaGNU,
# a rich and complex testing framework, but which is far from being standard on
# Posix systems. Worse yet, it was likely to be missing on the most fragile
# platforms, the very platforms that are most likely to torture Autoconf and
# exhibit deficiencies.
# "


########################################################################

# We cache a bunch of short strings, which we hope are nothing but the autotest result strings.
string_intern_cache: Dict[str, int] = { }

def ts_string_intern(db: sqlite3.Connection, name: str) -> int:
    global string_intern_cache
    if name in string_intern_cache:
        return string_intern_cache[name]

    db.execute("insert or ignore into autotest_string (name) values (?);", (name,))
    db.commit()
    # and fetch the testsuite record id right away
    sid: int = list(row[0] for row in db.execute("select id from autotest_string where name = ?", (name,)))[0]
    # logging.debug("found string %s interned-id %d", name, sid)
    if len(name) < 12:
        string_intern_cache[name] = sid
    return sid

def bytes_to_string(b: IO[bytes]) -> str:
    try:
        s = b.decode('utf-8')
    except:
        s = b.decode('iso-8859-1')
    return s.rstrip()

# Check if the .log file has a header similar to one of the follwing:
#
# Example header 1:
#
## ------------------------- ##
## debugedit 5.0 test suite. ##
## ------------------------- ##
#
# Example header 2:
#
## ----------------------------- ##
## GNU Autoconf 2.72 test suite. ##
## ----------------------------- ##
#
def is_autotest_logfile(db: sqlite3.Connection, logfile: git.objects.blob.Blob) -> bool:
    logfile_stream = tempfile.TemporaryFile()
    logfile.stream_data(logfile_stream)
    logfile_stream.seek(0)
    rv = True
    i=0
    s=''
    while True:
        i+=1
        b = logfile_stream.readline()
        if b == b'':
            break
        s = bytes_to_string(b)
        if i == 1:
            logging.debug("is_autotest_logfile line1 %s", s)
            if not re.search("^## --* ##$", s):
                rv = False
        if i == 2:
            logging.debug("is_autotest_logfile line2 %s", s)
            if not re.search("^## (.*)test suite. ##$", s):
                rv = False
            break
    logging.debug("is_autotest_logfile filename=%s rv=%s", logfile.name, str(rv))
    return rv

def record_result(db: sqlite3.Connection, trid: int, tstid: int, tcname: str, res: str, logcursor: int, errlog: int) -> None:
    resid = ts_string_intern(db, res)
    tcnameid = ts_string_intern(db, tcname)

    # Check for already recorded result.  A test result may be detected more
    # than one way in the test log. That's fine.  All the results the parser
    # detected for given testcase should be indentical though.  If we come
    # across a testcase for which parser found two different test results, then
    # we need to stop and check if parser works correctly.
    resid0 = None
    try:
        resid0: int = list(row[0] for row in db.execute("select result from autotest_testcase where testsuite = ? and tcname = ?", (tstid, tcnameid,)))[0]
    except:
        pass

    if resid0 is not None:
        if resid0 == resid:
            logging.debug("not re-recording test result {} for testcase {}".format(res, tcname))
            return
        else:
            # There apparently is a problem parsing the test log.  Either the
            # test log is inconsistent, or the parser misbehaves.
            print("PARSING ERROR: attempt to re-record test result for {} previus result was {} current test result is {}".format(tcname, resid0, resid))
            sys.exit()

    if errlog is None:
        db.execute("insert into autotest_testcase (testsuite, tcname, result, logcursor, errlog)"+
                   "values (?,?,?,?,NULL);",
                   (tstid, tcnameid, resid, logcursor))
    else:
        db.execute("insert into autotest_testcase (testsuite, tcname, result, logcursor, errlog)"+
                   "values (?,?,?,?,?);",
                   (tstid, tcnameid, resid, logcursor, errlog))

    db.commit()

def parse_autotest_commitish(db: sqlite3.Connection, commit: git.Repo.commit, trid: int) -> None:
    main_testlog: commit.tree = { }
    supportive_testlogs: Dict[int, int] = { }

    # Go through the uploaded files, identify main log.
    autotest_logfile_found = False
    for item in commit.tree.traverse():
        if (item.type == 'blob' and ".log" in item.name and is_autotest_logfile(db, item)):
            main_testlog = item
            autotest_logfile_found = True
            break

    if autotest_logfile_found == False:
        logging.debug("Skipping commitish {}.  No autotest testlog found.".format(commit.hexsha))
        return

    # We have the main log.  Let's try to find supportive logs the autotest
    # framework can produce to document failures in detail.  If these were
    # uploaded, benefit from them.
    for item in commit.tree.traverse():
        if (item.type == 'blob' and ".log" in item.name and "testsuite.dir/" in item.path):
            m = re.search("testsuite.dir/([0-9][0-9]*)/.*\\.log$", item.path)
            if m is not None:
                tcid = int(m.group(1))
                supportive_testlogs[tcid] = ts_string_intern(db, item.path)

    logging.debug("Checked commitish %s, found main autotest testlog %s and %d " + 
                  "supportive error log files.", commit.hexsha, main_testlog.path, len(supportive_testlogs))

    main_testlog_id = ts_string_intern(db, main_testlog.path)

    # create autotest_testsuite record; not more than one
    db.execute("insert into autotest_testsuite (tr, logfile) values (?, ?);",
               (trid, main_testlog_id))
    db.commit()

    # ... and find its id right away
    tstid = list(row[0] for row in db.execute("select id from autotest_testsuite where tr = ? and logfile = ?",
                                                      (trid, main_testlog_id)))[0]
    logging.debug("found testsuite id %d", tstid)

    logfile_stream = tempfile.TemporaryFile()
    main_testlog.stream_data(logfile_stream)
    logfile_stream.seek(0)

    logcursor = 0
    in_list_of_passes = False
    in_list_of_failures = False
    in_list_of_skips = False
    in_list_of_failure_details = False
    while True:
        testcase_id = -1
        testcase_name = ""
        testcase_res = ""
        bs = logfile_stream.readline()
        logcursor += 1
        if bs == b'':
            break
        l = bytes_to_string(bs)
        logging.debug("processing log file line: %s", l)
        if re.search("^testsuite: starting at:", l):
            in_list_of_passes = True
            continue
        if re.search("^testsuite: ending at:", l):
            in_list_of_passes = False
            continue
        if re.search("^## Summary of the failures", l):
            in_list_of_failures = True
            continue
        if re.search("^## Detailed failed tests", l):
            in_list_of_failures = False
            in_list_of_skips = False
            in_list_of_failure_details = True
            continue
        if re.search("## .*config.log.*##", l):
            in_list_of_failure_details = False
            continue
        if in_list_of_failures and re.search("^Skipped tests:", l):
            in_list_of_skips = True
            continue
        if in_list_of_passes:
            m = re.search("^([0-9][0-9]*). ([^\\(]*)\\([^.]*.at:[0-9][0-9]*\\): ok", l)
            if m is not None:
                testcase_id=int(m.group(1))
                testcase_name = m.group(2).strip()
                testcase_res = "PASS"
                record_result(db, trid, tstid, testcase_name, testcase_res, logcursor, None)
            # buildbot/debugedit-debian-ppc64/36
            m = re.search("^([0-9][0-9]*). ([^\\(]*)\\([^.]*.at:[0-9][0-9]*\\): skipped", l)
            if m is not None:
                testcase_id=int(m.group(1))
                testcase_name = m.group(2).strip()
                testcase_res = "SKIP"
                record_result(db, trid, tstid, testcase_name, testcase_res, logcursor, None)
            continue
        if in_list_of_skips:
            m = re.search("^ *([0-9][0-9]*): [^.]*.at:[0-9][0-9]* (.*)$", l)
            if m is not None:
                testcase_id = int(m.group(1))
                testcase_name = m.group(2).strip()
                testcase_res = "SKIP"
                record_result(db, trid, tstid, testcase_name, testcase_res, logcursor, None)
            continue
        if in_list_of_failures:
            m = re.search("^ *([0-9][0-9]*): [^.]*.at:[0-9][0-9]* (.*)$", l)
            if m is not None:
                testcase_id = int(m.group(1))
                testcase_name = m.group(2).strip()
                testcase_res = "FAIL"
                errlog = None
                if testcase_id in supportive_testlogs:
                    errlog = supportive_testlogs[testcase_id]
                record_result(db, trid, tstid, testcase_name, testcase_res, logcursor, errlog)
            continue

    logging.debug("finished processing the log file")
    logfile_stream.close()

def process_error_log_file(db: sqlite3.Connection, trid: int, tcid: int, logpath: str) -> None:
    resid = ts_string_intern(db, logpath)
    db.execute("update autotest_testcase set errlog=? where testsuite=? and tcid=?;", (resid, trid, tcid))
    db.commit()


########################################################################

def main() -> None:
    parser = argparse.ArgumentParser(description='Extract autotest test results.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--git',type=str,help='git testrun archive',default='.')
    parser.add_argument('--db',type=str,help='sqlite database file',default='bunsen.sqlite3')
    parser.add_argument('--loglevel',type=str,help='logging level',default='info')
    g = parser.add_mutually_exclusive_group()
    g.add_argument('--new',type=str,help='new testrun commit',default=None)
    g.add_argument('--dead',type=str,help='dead testrun commit',default=None)

    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel.upper(),
                        format="%(asctime)s:"+os.path.basename(__file__)+":%(levelname)s:%(message)s")
    logging.captureWarnings(True)

    # Open database, add our schema
    db = sqlite3.connect(args.db, uri=True)
    logging.debug('Opened sqlite3 db=%s', args.db)
    db.executescript("""
pragma synchronous=off;
pragma foreign_keys=on;
CREATE TABLE if not exists autotest_string (   /* string interning table */
       id integer primary key,
       name string unique);
CREATE TABLE if not exists autotest_testsuite (
       id integer primary key,
       tr integer not null,     /* testrun ref */
       logfile integer not null, /* e.g. dirname of tests/test-suite.log */
       foreign key (logfile) references autotest_string(id) on delete cascade,
       foreign key (tr) references testrun(id) on delete cascade);
CREATE INDEX if not exists autotest_ts_tr on autotest_testsuite(tr);
CREATE TABLE if not exists autotest_testcase (
       testsuite integer not null,
       tcname integer not null,
       result integer not null,
       logcursor integer not null,
       errlog integer default null,
       foreign key (testsuite) references autotest_testsuite(id) on delete cascade,
       foreign key (tcname) references autotest_string(id) on delete cascade,
       foreign key (errlog) references autotest_string(id) on delete cascade,
       foreign key (result) references autotest_string(id) on delete cascade);
DROP VIEW if exists autotest_testcase_v;
CREATE VIEW if not exists autotest_testcase_v (testsuite, tcname, tcnameid, result, logcursor, errlog, testlog) as
       select tc.testsuite, s2.name, tc.tcname, s3.name, tc.logcursor, s4.name, s5.name
       from autotest_testcase tc, autotest_string s2, autotest_string s3,
            autotest_testsuite at, autotest_string s5
       left join autotest_string s4
       on tc.errlog=s4.id
       where tc.tcname = s2.id and tc.result = s3.id
             and tc.testsuite=at.id and at.logfile=s5.id;
""")
    db.commit()


    if args.new:
        trid = list(row[0] for row in db.execute("select id from testrun where gitcommit = ?", (args.new,)))[0]
        logging.debug("found testrun commit %s, id %d", args.new, trid)

        # Drop preexisting rows in autotest_testsuite and autotest_testcase,
        # so we can create new rows without risk of duplication, even without
        # unique indexes.
        db.execute("delete from autotest_testsuite where tr=?", (trid,))
        db.commit()

        # Open the git repo
        testruns = git.Repo(args.git) #type: ignore[attr-defined]
        logging.debug('Opened git repo %s', args.git)
        commit = testruns.commit(args.new)
        logging.debug("found git commit %s", commit)

        # Parse the commit, record results to DB
        parse_autotest_commitish(db, commit, trid)

    if args.dead:
        logging.debug("auto-deleting autotest records for commit %s", args.dead)

if __name__ == '__main__':
    main()
