#! /usr/bin/env python3

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

# Compute summary statistics for recently discovered automake testsuites.


import git
import argparse
import sqlite3
import subprocess
import logging
import sys
import os
import tempfile
import re
from collections import defaultdict
from typing import DefaultDict

# globals
args : argparse.Namespace

def main() -> None:
    parser = argparse.ArgumentParser(description='Compute automake test result summaries for expfiles.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--git',type=str,help='git testrun archive',default='.')
    parser.add_argument('--db',type=str,help='sqlite database file',default='bunsen.sqlite3')
    parser.add_argument('--loglevel',type=str,help='logging level',default='info')
    g = parser.add_mutually_exclusive_group()
    g.add_argument('--new',type=str,help='new testrun commit',default=None) # XXX: take list
    g.add_argument('--dead',type=str,help='dead testrun commit',default=None)

    global args
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel.upper(),
                        format="%(asctime)s:"+os.path.basename(__file__)+":%(levelname)s:%(message)s")
    logging.captureWarnings(True)

    # Open database, add our schema
    db = sqlite3.connect(args.db, uri=True)
    logging.debug('Opened sqlite3 db=%s', args.db)
    db.executescript("""
pragma synchronous=off;
pragma foreign_keys=on;
CREATE TABLE if not exists automake_testsuite_summary (
       ts integer not null, /* testsuite ref */
       result integer not null, /* result name (interned) */
       number integer not null,
       unique (ts, result),
       foreign key (result) references automake_string(id) on delete cascade,
       foreign key (ts) references automake_testsuite(id) on delete cascade);
CREATE VIEW if not exists automake_testsuite_summary_v (ts, result, number) as
       select t.ts, rs.name, t.number
       from automake_testsuite_summary t, automake_string rs
       where t.result = rs.id;
""")
    db.commit()

    if args.new:
        trid = list(row[0] for row in db.execute("select id from testrun where gitcommit = ?", (args.new,)))[0]
        logging.debug("found testrun commit %s, id %d", args.new, trid)

        # The following could be done as a smaller set of more complex sql, or like this:
        tsids = db.execute("select testdir,id from automake_testsuite where tr = ?", (trid,))
        for testdir,tsid in tsids:
            logging.debug("found testsuite id %d, testdir %s", tsid, testdir)
            cur = db.cursor() # need separate cursor due to nested commits
            resultcounts = cur.execute("select tc.result,tcr.name,count(*) from automake_testcase tc, automake_string tcr " +
                                       "where tc.testsuite=? and tcr.id = tc.result group by tc.result;", (tsid,))
            
            resultc: DefaultDict[str, int] = defaultdict(lambda: 0)
            for (result,resultname,number) in resultcounts:
                logging.debug("found result %d count %d", result, number)
                db.execute("insert or ignore into automake_testsuite_summary (ts, result, number) values (?, ?, ?);",
                           (tsid,result,number))
                db.commit()
                resultc[resultname] += number
            logging.info("Counted testdir %s results %s", testdir, list(resultc.items()))
            cur.close()

    if args.dead:
        logging.debug("auto-deleting automake summary records for commit %s", args.dead)
        # database auto-drops automake_testsuite_expfile_summary rows by virtue of foreign key cascade

    # we may have caused the database to grow a lot, so optimize before shutdown
    db.execute("pragma optimize;")
    db.commit()

if __name__ == '__main__':
    main()
