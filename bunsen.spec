%global srcname bunsen

Name: bunsen
Version: 1.0.8
Release: 1%{?dist}
License: MIT
Summary: Bunsen is a test result storage and analysis toolkit
Url: https://sourceware.org/%{srcname}
Source0: %{name}-%{version}.tar.gz

BuildArch: noarch

Requires: python3-natsort
Requires: python3-flask
Requires: python3-tqdm
Requires: python3-GitPython
Requires: python3-typing-extensions

BuildRequires: make
BuildRequires: python3-natsort
BuildRequires: python3-flask
BuildRequires: python3-tqdm
BuildRequires: python3-GitPython
BuildRequires: python3-typing-extensions
BuildRequires: sqlite3
BuildRequires: jq
BuildRequires: lynx
BuildRequires: netcat
BuildRequires: wget2-wget

%description
Bunsen is a test result storage and analysis toolkit that collects test result
and build log files in a variety of formats (e.g. DejaGnu, Autoconf config.log,
glibc), stores them in a ludicrously compact de-duplicated Git repo, parses and
indexes the contents, and provides a toolkit for analyzing and browsing the
indexed test results. It's meant to complement automated testing systems such
as Buildbot that are great at launching tests but need additional functionality
to make detailed sense of the results.

%prep
%autosetup

%build
%configure
make %{?_smp_mflags}

%check
make check

%install
make DESTDIR=$RPM_BUILD_ROOT install

%files
%{_bindir}/g-dejagnu-cluster-entropy
%{_bindir}/g-gridview-test-summary
%{_bindir}/g-testrun-clusterfinder
%{_bindir}/i-autoconflog-parser
%{_bindir}/i-automake-parser
%{_bindir}/i-automake-summarizer
%{_bindir}/i-autotest-parser
%{_bindir}/i-autotest-summarizer
%{_bindir}/i-dejagnu-parser
%{_bindir}/i-dejagnu-summarizer
%{_bindir}/i-glibc-parser
%{_bindir}/i-glibc-summarizer
%{_bindir}/i-metadata-finder
%{_bindir}/i-omnitest-summarizer
%{_bindir}/i-testrun-indexer
%{_bindir}/pipeline
%{_bindir}/r-dejagnu-diff-logs
%{_bindir}/r-dejagnu-summary
%{_bindir}/r-dejagnu-testcase-classify
%{_bindir}/r-diff-testruns
%{_bindir}/r-find-testruns
%{_bindir}/r-find-testruns-with-commit
%{_bindir}/r-grid-testcase
%{_bindir}/r-httpd-browse
%{_bindir}/R-show-testcases
%{_bindir}/r-show-testrun
%{_bindir}/t-sourceware-mails-import
%{_bindir}/t-untag-testruns
%{_bindir}/t-upload-git-push
%{_mandir}/man1/pipeline.1.gz
%{_mandir}/man1/r-find-testruns.1.gz
%{_mandir}/man1/r-show-testrun.1.gz
%{_mandir}/man1/t-untag-testruns.1.gz
%{_mandir}/man7/bunsen.7.gz
%{_datadir}/bunsen/jstable.css
%{_datadir}/bunsen/common.py
%{_datadir}/bunsen/jstable.js
%{_datadir}/bunsen/robots.txt
%{_datadir}/bunsen/style.css
%{!?_licensedir:%global license %%doc}
%license mit.txt

%changelog
* Mon Feb 17 2025 Martin Cermak <mcermak@redhat.com> 1.0.8-1
- PR32617: Autoreconf (mcermak@redhat.com)
- PR32617: remove --list-branches from r-find-testruns-with-commit code
  (mcermak@redhat.com)
- PR32526: Add hyperlinks to the clusters overview (mcermak@redhat.com)
- i-autoconflog-parser: extract AC_INIT package name/version values
  (fche@redhat.com)
- ditto, jinja syntax fix (fche@redhat.com)
- PR32526: also render source.gitdescribe metadata value into gitweburl
  (fche@redhat.com)
- Drop a forgotten stub code snippet (mcermak@redhat.com)
- Fix r_grid_testcase's webui in case of timeout / error rc / empty output
  (mcermak@redhat.com)
- PR32526:  Use --git repo for reading gitconfig (mcermak@redhat.com)
- PR32526: Add href'ing to the r-diff-testruns web stuff (mcermak@redhat.com)
- PR32526: r-httpd-browse: support upstream-git commit outgoing href'ing
  (mcermak@redhat.com)
- t-untag-testruns: older python compat tweak (fche@redhat.com)
- PR32609: t-untag-testruns --keep-upstream-tagged option, testrun.git_describe
  rename (fche@redhat.com)

* Thu Jan 23 2025 Martin Cermak <mcermak@redhat.com> 1.0.7-1
- Add BuildRequires: python3-typing-extensions (mcermak@redhat.com)

* Thu Jan 23 2025 Martin Cermak <mcermak@redhat.com> 1.0.6-1
- Add BuildRequires for tests/run-r-httpd-browse.sh (mcermak@redhat.com)

* Thu Jan 23 2025 Martin Cermak <mcermak@redhat.com> 1.0.5-1
- Add sqlite3 and jq to BuildRequires (mcermak@redhat.com)

* Thu Jan 23 2025 Martin Cermak <mcermak@redhat.com> 1.0.4-1
- Run testsuite at the RPM build time (mcermak@redhat.com)
- Add r-find-testruns-with-commit --exact as a shorthand for --maxdelta=0
  (mcermak@redhat.com)
- Drop the r-find-testruns-with-commit --maxdepth swith (mcermak@redhat.com)
- Improve tests/run-r-grid-testcase.sh and fix related issues
  (mcermak@redhat.com)
- r-grid-testcase:  Interpret h/v limits as horizontal / vertical span
  (mcermak@redhat.com)
- A recent commit accidentally broke html nesting and disabled these buttons.
  (fche@redhat.com)
- r-find-testruns: accelerate --with-expfile* query (fche@redhat.com)
- r-find-testruns-with-commit: also emit gitrepo clone path in json output
  (fche@redhat.com)
- r-show-testrun: apply sort to textual output (fche@redhat.com)
- r-httpd-browse.in: Explain the gridview test results aggregation
  (mcermak@redhat.com)
- r-httpd-browse.in: Group search forms into tabbed layout (mcermak@redhat.com)
- r-httpd-browse.in: Fix a JS syntax error (mcermak@redhat.com)
- Make r-show-testrun output deterministic (fix tests/run-autotest.sh)
  (mcermak@redhat.com)
- Allow for running tests/run-r-httpd-browse.sh uninstalled
  (mcermak@redhat.com)
- README: Update the rebuild steps (mcermak@redhat.com)
- README:  How to do a COPR build (mcermak@redhat.com)
- r-find-testruns-with-commit: Call out to git-rev-parse (fche@redhat.com)

* Fri Dec 13 2024 Martin Cermak <mcermak@redhat.com> 1.0.3-1
- Requires: python3-natsort python3-flask python3-tqdm python3-GitPython
  python3-typing-extensions (mcermak@redhat.com)

* Fri Dec 13 2024 Martin Cermak <mcermak@redhat.com> 1.0.2-1
- BuildRequire: make 

* Fri Dec 13 2024 Martin Cermak <mcermak@redhat.com> 1.0.1-1
- bunsen.spec: BuildRequires: python3-GitPython (mcermak@redhat.com)

* Fri Dec 13 2024 Martin Cermak <mcermak@redhat.com> 1.0.0-1
- new package built with tito

* Thu Dec 12 2024 Martin Cermak <mcermak@redhat.com> - 0.0-0
- Genesis
