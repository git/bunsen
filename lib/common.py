#! /usr/bin/env python3

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022-2025 Red Hat, Inc.

# Common functions used by other scripts

import os
import sys
import git
import time
import hashlib
import logging
import subprocess
from typing import List
import concurrent.futures

# get_gitconfig_refreshsecs() returns timeout value (secs) for refresh
# of local caches of remote repositories.  Example configuration in 
# ~/.gitconfig:
#
# [bunsentiming]
#    refreshsecs = 300
#
def get_gitconfig_refreshsecs (bunsengit: str) -> int :
    g = git.cmd.Git(bunsengit) # find configuration in testrun git repo
    configs = g.config('--list','-z').split('\0')
    for c in configs:
        try:
            key,value = c.split('\n')
            keysplit = key.split('.')
            if keysplit[0] == 'bunsentiming' and keysplit[1] == 'refreshsecs':
                rv = int(value)
                logging.debug(f"using configured refreshsecs value ({rv})")
                return rv
        except:
            pass
    # if config not found, return a default
    rv = 15 * 60
    logging.debug(f"using default refreshsecs value ({rv})")
    return rv

# Canonicalize source giturl based on git-config, apprx. same algorithm
# as git remote.c's "git remote get-url".  NB: this not
# "git config --get-urlmatch", which maps in the wrong direction.
#
# [url "URL1"]
#    insteadOf = URL2
#
def canonicalize_git_repo (bunsengit: str, URL2: str) -> str :
    g = git.cmd.Git(bunsengit) # find configuration in testrun git repo
    configs = g.config('--list','-z').split('\0')
    lhs = ""
    rhs = ""
    for c in configs:
        try:
            key,value = c.split('\n')
            keysplit = key.split('.')
            if keysplit[0].lower() == 'url' and keysplit[-1].lower() == 'insteadof':
                logging.debug(f"considering {keysplit} {value}")    
                if URL2.startswith(value) and len(value) > len(lhs): # find longest-prefix match
                    lhs = value
                    rhs = '.'.join(keysplit[1:-1])
                    logging.debug(f"possibly rewrite lhs {lhs} rhs {rhs}")
        except:
            pass
    if lhs != "": 
        newurl = rhs + URL2[len(lhs):] # rewrite prefix
        logging.debug(f"rewrote {URL2} to {newurl}")
    else:
        newurl = URL2
    assert newurl != ""
    return newurl


# get fresh list of git commit hashes for given git url.
def get_git_repo(logging: logging.Logger, forcerefresh: bool,
                 skiprefresh: bool, refreshsecs: int, cachedir: str,
                 bunsengit: str, upstreamgit: str) -> git.repo.base.Repo:
    upstreamgit = canonicalize_git_repo (bunsengit, upstreamgit)
    logging.debug(f"started get_git_repo() for {upstreamgit}")

    if upstreamgit.startswith('/') or upstreamgit.startswith('file:'): # local shortcut, no mirror/cache!
        try:
            return git.Repo(upstreamgit)
        except:
            logging.error(f"failed accessing {upstreamgit}")
            sys.exit(1)
    
    myhash = hashlib.md5(upstreamgit.encode('utf-8')).hexdigest()
    cachedir = cachedir.rstrip("/")
    workdir = f"{cachedir}/refrepo-{myhash}"
    if forcerefresh:
        logging.debug(f"removing cache for {upstreamgit} ({workdir})")
        try:
            subprocess.run(["rm", "-rf", workdir])
        except:
            logging.error(f"failed removing {workdir}")
            sys.exit(1)
    if not os.path.exists(workdir):
        os.makedirs(workdir)
    myrepo = False
    logging.debug(f'attempting to open git repo {workdir} mirroring {upstreamgit}')
    try:
        myrepo = git.Repo(workdir)
    except git.exc.InvalidGitRepositoryError:
            logging.debug(f'cloning {upstreamgit} into {workdir}...')
            res = subprocess.run(["git", "clone", "--filter=blob:none", "--no-checkout",
                                  upstreamgit, workdir], check=True)
    if not myrepo:
        try:
            myrepo = git.Repo(workdir)
        except:
            logging.error(f"failed setting up a mirror {workdir} of repo {upstreamgit}")
            sys.exit(1)

    # check if repo needs refresh
    need_refresh = True
    if os.path.isfile(f"{workdir}/.git/FETCH_HEAD"):
        freshness = int(time.time() - os.path.getmtime(f"{workdir}/.git/FETCH_HEAD"))
        need_refresh = (freshness > refreshsecs)
        logging.debug(f"repo last refreshed {freshness} seconds ago, need_refresh:{str(need_refresh)}")

    if not skiprefresh and need_refresh:
        try:
            myrepo.remotes.origin.fetch()
        except:
            logging.error(f"failed fetching {upstreamgit}")
    logging.debug(f"finished get_git_repo() for {upstreamgit}")
    return myrepo;

def __get_git_repo(logging: logging.Logger, forcerefresh: bool,
                   skiprefresh: bool, refreshsecs: int, cachedir: str,
                   bunsengit: str, upstreamgit: str) -> (str, git.repo.base.Repo):
    try:
        rv = get_git_repo(logging, forcerefresh, skiprefresh, refreshsecs,
                          cachedir, bunsengit, upstreamgit)
        return (upstreamgit, rv)
    except:
        logging.exception(f"failed fetching {upstreamgit} branches")
    return None

def get_multi_git_repos(logging: logging.Logger, forcerefresh: bool,
                        skiprefresh: bool, refreshsecs: int, cachedir: str,
                        bunsengit: str, upstreamgits: List[str]) -> List[git.repo.base.Repo]:
    params = [(logging, forcerefresh, skiprefresh, refreshsecs, cachedir, bunsengit, u['url']) for u in upstreamgits]
    with concurrent.futures.ThreadPoolExecutor() as executor:
        results = executor.map(lambda p: __get_git_repo(*p), params)
    return [r for r in list(results) if r is not None]
