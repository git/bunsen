'\" t macro stdmacro

.TH "BUNSEN PIPELINE" "1"

.SH "NAME"
pipeline \- Run bunsen testrun analysis pipeline

.SH "SYNOPSYS"
.B pipeline
[\fIOPTION\fP]... [\fICOMMIT\fP]...

.SH "DESCRIPTION"

The \fBpipeline\fP process responds to changes in a testrun git repo
relative to the last run of the bunsen pipeline.  It runs incremental
analysis engines for each new testrun commit, cleans up after dead
testrun commits, and runs global analysis engines after the prior
changes are complete.  See the "ENGINES" sections below for a list of
analysis engines, and what tables in the sqlite database they process.

If the testrun git repo has not materially changed, \fBpipeline\fP
does nothing, just reports some statistics.

If the analysis process is interrupted with a signal, \fBpipeline\fP
attempts to delete the testrun data currently being processed, so the
next run will treat it as new, and rerun analysis from scratch.

.SH "OPTIONS"

.TP
.B "\-\-git \fIPATH\fP"
Designate the git repository where testruns are stored.  This must be
a local directory containing a bare or working-tree clone, rather than
a remote repository URL.  A few tools modify this repository, but most
treat it as read only.  The default is the current working directory.

.TP
.B "\-\-db \fiDB\fP"
Designate the sqlite database file containing bunsen analysis results.
The default is \fIbunsen.sqlite3\fP.

.TP
.B "\-h" "\-\-help"
Dangerous option, use sparingly.

.TP
.B "\-\-loglevel \fILOGLEVEL\fP"
Set the diagnostic log level for the process.  The available modes
include DEBUG, INFO, WARNING, ERROR, CRITICAL, or the equivalent
numbers or lower case forms, as per the python3 "logging" library.
The default is "info".

.TP
.B "\-k" "\-\-keep\-going"
In case of an error or interrupt signal, \fBpipeline\fP normally stops
invoking incremental engines for other new/dead testruns, so as to
stop quickly.  With this option, it will continue incremental engine
processing for other testruns, analogously to "make -k".  The
interrupted analysis may be reattempted the next time around.

.TP
.B "\-r" "\-\-redo"
Normally, \fBpipeline\fP ignores testruns in the git repository that have
already been analyzed into the sqlite database.  With this option, old
testrun commits will be reanalyzed as if they were new.  This may be useful
if the bunsen version has changed, and new or modified analysis for older
testruns is desired.

.TP
.B "\-d" "\-\-dead"
With this option, \fBpipeline\fP treats all testrun commits as dead,
so as to trigger removal of their analysis results.

.TP
.B "\-\-i\-engine \fII_ENGINE\fP" "\-\-g\-engine \fIG_ENGINE\fP"
Override the default list of incremental and/or global analysis engines
invoked by \fBpipeline\fP.  This may be useful in connection with
\fB\-\-redo\fP.  The analysis engines are separate programs, but since
they are intended for invocation by the pipeline rather than the user,
they are not documented in individual detail.  See the "ENGINES"
section below.

.TP
.B "\-\-refspec \fIGLOB\fP"
Normally, \fBpipeline\fP processes all commits in the testrun git
repo.  With this option, \fBpipeline\fP limits it attention only to
testrun commits whose tags match the given git refspec.  This may be
useful if the git repo contains other commits known not to be relevant
to bunsen.

.TP
.B \fICOMMIT ...\fP
Normally, \fBpipeline\fP processes all commits in the testrun git
repo.  With this option, \fBpipeline\fP limits its attention only to
the given testrun commits.  This may be useful in connection with
\fB\-\-redo\fP and \fB\-\-dead\fP.

.SH "INCREMENTAL ANALYSIS ENGINES"

These engines are invoked, in sequence, for each new or dead testrun
commit.

.TP
i-testrun-indexer
Takes notice of a testrun commit, and manages basic
placeholder records and initial set of metadata.
Updates \fBtestrun\fP and \fBtestrun_kv\fP tables.

.TP
i-metadata-finder
Extracts additional metadata from \fI.bunsen\fP files.
Updates the \fBtestrun_kv\fP table.

.TP
i-dejagnu-parser
Parses \fI*.sum\fP and \fI*.log\fP files produced by dejagnu test runs.
The \fI*.sum\fP files are mandatory; \fI*.log\fP may be absent.  This
parser tolerates re-sorted \fI*.sum\fP files, a peculiar step done by
some gnu toolchain testsuites.  Updates the \fBdejagnu_testsuite\fP
table to represent individual ".exp" stanzas, and the
\fBdejagnu_testcase\fP table to represent each individual subtest,
plus \fBdejagnu_string\fP as a string-interning table.  Extracts
additional metadata into \fBtestrun_kv\fP.  See also the
\fBdejagnu_testcase_v\fP view.

.TP
i-dejagnu-summarizer
Processes and aggregates the \fBdejagnu_testsuite\fP and
\fBdejagnu_testcase\fP tables to produce
\fBdejagnu_testsuite_expfile_summary\fP, which contains counts of
categories of test results.  See also the
\fBdejagnu_testsuite_expfile_summary_v\fP view.

.TP
i-glibc-parser
Parses \fI*.test-result\fP and \fI*.out\fP files produced by glibc "make
check" runs.  The \fI*.test-result\fP files are mandatory; \fI*.out\fP
may be absent.  Updates the \fBglibc_testsuite\fP table to represent
individual test directories, and the \fBglibc_testcase\fP table to
represent each individual subtest, plus \fBglibc_string\fP as a
string-interning table.  Extracts additional metadata into
\fBtestrun_kv\fP.  See also the \fBglibc_testcase_v\fP view.

.TP
i-glibc-summarizer
Processes and aggregates the \fBglibc_testsuite\fP and
\fBglibc_testcase\fP tables to produce
\fBglibc_testsuite_summary\fP, which contains counts of
categories of test results.  See also the
\fBglibc_testsuite_summary_v\fP view.

.TP
i-automake-parser
Parses \fItest-suite.log\fP, \fI*.trs\fP and \fI*.out\fP files
produced by automake "make check" runs.  The \fI*.trs\fP files
are mandatory; \fI*.out\fP may be absent.  Updates the \fBautomake_testsuite\fP table to represent
individual test directories, and the \fBautomake_testcase\fP table to
represent each individual subtest, plus \fBautomake_string\fP as a
string-interning table.  Extracts additional metadata into
\fBtestrun_kv\fP.  See also the \fBautomake_testcase_v\fP view.

.TP
i-automake-summarizer
Processes and aggregates the \fBautomake_testsuite\fP and
\fBautomake_testcase\fP tables to produce
\fBautomake_testsuite_summary\fP, which contains counts of
categories of test results.  See also the
\fBautomake_testsuite_summary_v\fP view.

.TP
i-autoconflog-parser
Processes all the \fIconfig.log\fP files produced during autoconf's
probing of a compilation environment.
Updates the \fBautoconflog_testsuite\fP table to represent
individual test directories, and the \fBautoconflog_testcase\fP table to
represent each individual subtest and check result, plus \fBautoconflog_string\fP as a
string-interning table.  Extracts additional metadata into
\fBtestrun_kv\fP.  See also the \fBautoconflog_testcase_v\fP view.

.TP
i-omnitest-summarizer
Summarizes the testsuite summaries into overall counts of
PASS/FAIL/etc. results.
Adds a string representation as \fBomnitest.results\fP metadata
within \fBtestrun_kv\fP.
See also the \fBomnitest_summary_v\fP view.

.SH "GLOBAL ANALYSIS ENGINES"

These engines are invoked once, in sequence, after all the incremental
engines for a pipeline operation are done.

.TP
g-testrun-clusterfinder
Groups all testruns into clusters.  Each cluster of testruns is
defined by sharing a particular key/value tuple pair from
\fBtestrun_kv\fP.  Then, clusters are related to one another by
natural-order sorting the value strings for each key.  The result
is that each cluster is linked to the previous / next cluster,
being the one that has the previous / next value for that key.  Updates
the \fBtestrun_cluster\fP, \fBtestrun_cluster_member\fP tables.
See also the \fBtestrun_cluster_size\fP view.

.TP
g-dejagnu-cluster-entropy
Estimates a information-theoretical entropy for dejagnu ".exp" test
files, by measuring the variation of subtest result counts (number of
PASS, FAIL, etc. results, based on the
\fBdejagnu_testsuite_expfile_summary\fP table), for all testruns in
any given cluster.  Low entropy indicates less variation, meaning
testruns tend to have the same results.  Zero entropy means boringly
identical results.  High entropy indicates higher variation among
testruns.  This pass is relatively slow and not run by default.
Updates the \fBtestrun_cluster_dejagnu_expfile_entropy\fP table.  See
also the \fBtestrun_cluster_dejagnu_expfile_entropy_v\fP view.

.SH "COPYRIGHT"

Copyright \(co 2019-2022 Red Hat, Inc.
SPDX-License-Identifier: LGPL-3.0-or-later


.SH "SEE ALSO"
.PP
\fBbunsen\fP(7)
\fBgit\fP(1)
\fBdejagnu\fP(1)
\fBautomake\fP(1)
\fBautoconf\fP(1)
\fBgitrepository-layout\fP(8)
\fBsqlite3\fP(1)
