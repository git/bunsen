'\" t macro stdmacro


.de SAMPLE
.br
.RS 0
.nf
.nh
..
.de ESAMPLE
.hy
.fi
.RE
..

.TH "BUNSEN" "7"

.SH "NAME"
bunsen \- Test log analysis tools

.SH "DESCRIPTION"
Bunsen is a collection of tools for bulk storage, analysis, and reporting
of textual testsuite results.  This page describes overall architecture.
The typical processing flow is straightforward.  Text in boxes
represents operations, text not in boxes represents data.


.nf
┌──────────────┐                     ┌──────────────┐
│run testsuite ├──────┐        ┌─────┤ add metadata │
└──────────────┘      ▼        ▼     └──────────────┘
                    test log files
                          │
                          ▼
                 ┌──────────────────┐
                 │import into bunsen│
                 └────────┬─────────┘
                          │
                          ▼
               bunsen testrun git repo
                          │
                          ▼
                ┌───────────────────┐
                │run bunsen pipeline│
                └────┬─────┬────────┘
                     │  ▲  │  ▲
                     ▼  │  ▼  │
                bunsen sqlite database
                          │
                          ▼
               ┌──────────────────────┐
               │collect bunsen reports│
               └──┬───────┬────────┬──┘
                  │       │        │
                  ▼       ▼        ▼
                cli      html    tools
.fi


.SS "run testsuite"
This is up to you: maybe a \fI"make check"\fP?

See \fBdejagnu\fP(1) for example.

.SS "add metadata"

It may be desirable to add some \fImetadata\fP, if the log files don't
sufficiently describe your test environment.  Bunsen accepts optional
string key/value tuples in \fB.bunsen*\fP text files.  While these
tuples are arbitrary, some later bunsen analysis passes interpret some
specially.  For example, consider including this early in your
testsuite Makefile or equivalent:

.SAMPLE
   (cd $srcdir; git rev-parse HEAD) > .bunsen.source.gitname
   (cd $srcdir; git describe --tags HEAD) > .bunsen.source.gitdescribe
   (cd $srcdir; echo $BRANCH) > .bunsen.source.gitbranch
   (cd $srcdir; echo $REPOURL) > .bunsen.source.gitrepo
.ESAMPLE

If a previous testsuite run is incomplete or crashed, but still worth
processing in bunsen, consider adding this metadata:

.SAMPLE
   echo crashed > .bunsen.completion
.ESAMPLE

.SS "test log files"

The result of the testsuite and metadata operations is simply a
collection of text files sitting in directories on your existing test
infrastructure.  Bunsen does not mandate any particular file layout,
except that later analysis is usually triggered by filename patterns.
Unnecessary renaming or moving should be avoided.

.SS "import into bunsen"

This step collects a snapshot of all those files by adding them all as
a new git commit into a designated git repo.  We call each commit a
"testrun", and it will be uniquely identified by its git commit hash,
and also described by its metadata.  Some complications include:

.IP 1.
having to authenticate so as to push to a remote git repo.  An
automated test system may need ssh keys distributed for this purpose.
It may be wise to create userids at the testrun git server that permit
new commits only.

.IP 2.
having to upload via non-git-native protocols such as HTTP.  You may
use any toolset to transmit your log files to the git server, rsync,
tar, curl, whatever.

Bunsen includes several scripts to help with the act of pushing or
uploading collections of test log files into your testrun git repo.
Check the \fBt-upload-git-push\fP tool and its siblings.

.SS "bunsen testrun git repo"

The bunsen testrun git repo is simply an unstructured collection of
commits, each containing test result and metadata text files.  The
commits can exist in a branch, or they can be orphan commits (with
some arbitrary tag, so it's protected from garbage collection).

You may untag such a commit or delete a branch, then run \fIgit gc\fP,
to delete the related text files from history.  Bunsen will later
notice this and erase related analysis.

You may use a set of git repo clones to federate and redistribute test
result commits.  You may update clones manually or by automation.
Because test case commits can be on orphan tags/branches, the clones
can be partial, as opposed to full-history full-depth.

No matter how the data gets there, a clone of the final testrun git
repo needs to be made available for local reading by bunsen.

See \fBgitrepository-layout\fP(8), \fBbunsen-testrun-repo\fP(5) for
more details.

.SS "run bunsen pipeline"

Bunsen takes things from here.  Run the \fBpipeline\fP program,
passing it the git testrun repo and an sqlite database file name.  The
pipeline program will notice what has changed in the testrun git repo
(new or dead commits).

Then the \fBpipeline\fP(1) runs a configured sequence of analysis
engines.  These engines are distinct programs named \fBi-*\fP or
\fBg-*\fP.  Some analysis engines are "incremental", so they consume
individual new and/or dead testrun commits.  Others are "global", run
to perform derived analysis based on all test log commits.  These are
run last.

Each analysis engine is a separate program registered with the
pipeline.  Each engine consumes the testrun git repo and/or the sqlite
data, and produces new sqlite data.  This new data is typically in new
tables managed specially by each engine.  Sometimes, new data is in
the form of additional rows in a common table, such as those related
to testrun key/value metadata.

.SS "bunsen sqlite database"

Bunsen stores analysis results in a single sqlite database.  This is
usually a single file.  The sqlite database should be located on local
high-speed storage.  It will be busy.

Since this data is entirely derived from the testrun git repo and
perhaps auxiliary data, the sqlite database itself is expendable.  If
it's erased or lost, the bunsen pipeline will recompute it all.

The schema of this database is determined by the analysis engines in
the pipeline.  If the bunsen pipeline code changes, the schema may
change.  The \fBpipeline\fP man page summarizes the engines and the
database tables & views they maintain.

You may use normal sqlite tools and API bindings to back up, view,
and to run ad-hoc queries against the data.  Avoid modifying the data.

See \fBsqlite3\fP(1) for more details.

.SS "collect bunsen reports"

Once analysis is complete, or even while analysis is occurring,
results may be queried.  Bunsen will include several reporting tools,
including command line tools, interactive web apps, notification
generators.  These will be individually documented.  These scripts
are named \fBr-*\fP (report).

The reporting tools read sqlite data, and thus are sensitive to the
schema produced by the analysis engines.  Careful management of code
evolution will be necessary, if one does not wish to remove previous
databases and recompute them with newer versions of bunsen tools.

.SS "retention control"

To prevent monotonic accumulation of content, old data should likely
be cleaned out.  It is sufficient to untag the testrun commits in the
git repo.  Subsequent pipeline runs remove related sqlite data, and
\fBgit-gc\fP runs gradually clean out the related git objects.  The
\fBt-untag-testruns\fP(1) script lets one automate the untagging by
criteria such as age and size.

.SH "COPYRIGHT"

Copyright \(co 2019-2022 Red Hat, Inc.
SPDX-License-Identifier: LGPL-3.0-or-later

.SH "BUNSEN TOOLS"
\fBpipeline\fP(1)
\fBt-untag-testruns\fP(1)
\fBr-show-testrun\fP(1)
\fBr-find-testruns\fP(1)

.SH "SEE ALSO"
.PP
\fBgit\fP(1)
\fBdejagnu\fP(1)
\fBgitrepository-layout\fP(8)
\fBsqlite3\fP(1)
