LABEL org.opencontainers.image.authors="bunsen@sourceware.org"
LABEL org.opencontainers.image.url="https://sourceware.org/bunsen/"
LABEL org.opencontainers.image.title="Bunsen testsuite log storage/analysis tools"
LABEL org.opencontainers.image.description="Complete bunsen toolkit, installed in /usr/local, "

FROM registry.fedoraproject.org/fedora:37

# Install bunsen buildreqs
RUN dnf -y install shadow-utils make rpm-build git jq sqlite python3-jinja2 python3-flask python3-natsort python3-GitPython python3-tqdm python3-pip && dnf -y update && dnf -y clean all

# Fetch & build bunsen from sourceware
RUN useradd bunsen
USER bunsen
WORKDIR /home/bunsen/source
RUN git clone https://sourceware.org/git/bunsen.git .
RUN ./configure
RUN make all
RUN git config --global user.email "nobody@example.com"
RUN git config --global user.name "Container User"
RUN git config --global --add safe.directory /home/bunsen/testruns
RUN make check || true
RUN cat tests/test-suite.log

USER root
RUN make install
USER bunsen
RUN mkdir /home/bunsen/testruns /home/bunsen/database
RUN pip3 install torch

# Export 
VOLUME /home/bunsen/testruns
VOLUME /home/bunsen/database

# Container invocation: run the bunsen pipeline, then start the web server
EXPOSE 8003
USER bunsen
CMD if [ ! -d /home/bunsen/testruns/.git ]; then git init /home/bunsen/testruns; fi; pipeline --git /home/bunsen/testruns --db /home/bunsen/database/bunsen.sqlite3; r-httpd-browse --git /home/bunsen/testruns --db /home/bunsen/database/bunsen.sqlite3

# HEALTHCHECK  CMD curl -f http://localhost:8083/home/bunsen/testruns/ || exit 1
