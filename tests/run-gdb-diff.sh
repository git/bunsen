#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db --loglevel=debug

# Import the test files
tag1=arbitrary1
commit1=`(cd $abs_srcdir/testdata/gdb-keiths; env GIT_AUTHOR_DATE="1970-01-01T01:01" t-upload-git-push $gitrepo $tag *) | awk '{print $1}' `
tag2=arbitrary2
commit2=`(cd $abs_srcdir/testdata/gdb-keiths2; env GIT_AUTHOR_DATE="2000-01-01T01:01" t-upload-git-push $gitrepo $tag2 *) | awk '{print $1}' `

# Rerun the bunsen pipeline
pipeline --git $gitrepo --db $db --loglevel=debug --i-engine i-testrun-indexer --i i-metadata-finder --i i-dejagnu-parser --i i-dejagnu-summarizer --i i-automake-parser --i i-autoconflog-parser

cat <<EOF > results-expected.txt

	$commit1
		$commit2

metadata diffs

omnitest.date
	2022-04-14
		2023-04-14

testrun.authored.day
	1970-01-01
		2000-01-01

testrun.authored.time
	1970-01-01 01:01
		2000-01-01 01:01

testrun.gitdescribe
	foobar.log
		arbitrary2

dejagnu diffs

gdb.gdb/test_outcomes.exp [native-gdbserver/-m32] gdb.gdb/test_outcomes.exp: expect FAIL
	FAIL	gdb.log:75 gdb.sum:14
		PASS	gdb.log:69 gdb.sum:14

gdb.gdb/test_outcomes.exp [native-gdbserver/-m32] gdb.gdb/test_outcomes.exp: expect UNRESOLVED
	UNRESOLVED	gdb.log:67 gdb.sum:19
		FAIL	gdb.log:74 gdb.sum:19
EOF
r-diff-testruns --git $gitrepo --db $db --dgexpfile 'gdb.gdb/test_outcomes.exp [native-gdbserver/-m32]' $commit1 $commit2 | tee results-actual.txt
diff results-expected.txt results-actual.txt

cat <<EOF > results-expected.txt

	$commit1
		$commit2

metadata diffs

omnitest.date
	2022-04-14
		2023-04-14

testrun.authored.day
	1970-01-01
		2000-01-01

testrun.authored.time
	1970-01-01 01:01
		2000-01-01 01:01

testrun.gitdescribe
	foobar.log
		arbitrary2

dejagnu diffs

gdb.gdb/test_outcomes.exp [native-gdbserver/-m32] gdb.gdb/test_outcomes.exp: expect FAIL
	FAIL	gdb.log:75 gdb.sum:14
		PASS	gdb.log:69 gdb.sum:14
EOF

r-diff-testruns --git $gitrepo --db $db --loglevel=debug --regressions $commit1 $commit2 | tee results-actual.txt
diff results-expected.txt results-actual.txt

cat <<EOF > results-expected.txt

	$commit2
		$commit1

metadata diffs

omnitest.date
	2023-04-14
		2022-04-14

testrun.authored.day
	2000-01-01
		1970-01-01

testrun.authored.time
	2000-01-01 01:01
		1970-01-01 01:01

testrun.gitdescribe
	arbitrary2
		foobar.log
EOF

r-diff-testruns --git $gitrepo --db $db --loglevel=debug --regressions $commit2 $commit1 | tee results-actual.txt
diff results-expected.txt results-actual.txt

# Test json format and dupe commit codes
cat <<EOF > results-expected.txt
[]
[
  "$commit1",
  "$commit1"
]
EOF

# Collect the two commit hashes
r-diff-testruns --git $gitrepo --db $db --template json $commit1 $commit1 | jq .dejagnu_results,.testruns | tee results-actual.txt

diff results-expected.txt results-actual.txt


# Check out r-find-testruns

cat <<EOF > results-expected.txt
[
  {
    "order_by": "2000-01-01",
    "testrun": "$commit2"
  },
  {
    "order_by": "1970-01-01",
    "testrun": "$commit1"
  }
]
EOF
r-find-testruns --git $gitrepo --db $db --template json --order-by testrun.authored.day --loglevel debug | jq .testruns | tee results-actual.txt
diff results-expected.txt results-actual.txt

cat <<EOF > results-expected.txt
$commit2 native-gdbserver/-m32
EOF
r-find-testruns --git $gitrepo --db $db --loglevel debug --order-by "" --has-keyvalue target_board '%' --has-keyvalue-op 'like' --has-expfile-like '%fake_test.exp%' | tee results-actual.txt
diff results-expected.txt results-actual.txt

r-find-testruns --git $gitrepo --db $db --loglevel debug --order-by "" --has-keyvalue target_board '*' --has-keyvalue-op 'glob' --has-expfile-like '%fake_test.exp%' | tee results-actual.txt
diff results-expected.txt results-actual.txt

# sqlite3 $db 'select * from testrun_kv;'

r-find-testruns --git $gitrepo --db $db --loglevel debug --order-by "" --has-keyvalue target_board 'native-gdbserver/-m32' --has-keyvalue-op '=' --has-expfile-like '%fake_test.exp%' | tee results-actual.txt
diff results-expected.txt results-actual.txt



# text --has-expfile and --has-trsfile together
cat <<EOF > results-expected.txt
$commit1
EOF
r-find-testruns --git $gitrepo --db $db --loglevel debug --order-by "" --has-expfile-like '%' --has-trsfile-like '%' | tee results-actual.txt
diff results-expected.txt results-actual.txt

# check a bit of clustering
cat <<EOF > results-expected.txt
"2000-01-01"
EOF
r-show-testrun --clusters --git $gitrepo --db $db --loglevel debug $commit1 --template json | jq '.cluster_k_next_v."testrun.authored.day"' | tee results-actual.txt
diff results-expected.txt results-actual.txt

cat <<EOF > results-actual.txt
1|architecture|1.0
2|compiler_info|1.0
3|gdb_version|1.0
6|target_board|1.0
EOF

g-dejagnu-cluster-entropy --git $gitrepo --db $db --update  # these should be instant and do nothing
g-dejagnu-cluster-entropy --git $gitrepo --db $db --update
g-dejagnu-cluster-entropy --git $gitrepo --db $db --update
sqlite3 $db 'select cluster,key,entropy from testrun_cluster_dejagnu_expfile_entropy_v where entropy > 0' | tee results-expected.txt
diff results-expected.txt results-actual.txt

exit 0
