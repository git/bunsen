#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

export PATH=$abs_srcdir/../bin:$PATH
export GIT_AUTHOR_DATE="1970-01-01T01:01"

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo


# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/systemtap/001; t-upload-git-push $gitrepo $tag systemtap.{sum,log})

# Rerun the bunsen pipeline
pipeline --git $gitrepo --db $db

# testcase 1.1

commitish=$(r-find-testruns --git $gitrepo --db $db --template json | jq '.testruns[0].testrun' | tr -d '"')

r-show-testrun --git $gitrepo --db $db --autotest $commitish | tee r-show-testrun.txt

cat <<EOF > r-show-testrun-ref.txt
testrun: $commitish

metadata:
architecture:	x86_64
omnitest.date:	2024-11-13
omnitest.results:	UNTESTED=270 PASS=267 FAIL=17 ERROR=3 UNRESOLVED=1
target_board:	unix
testrun.authored.day:	1970-01-01
testrun.authored.time:	1970-01-01 01:01
testrun.gitdescribe:	$tag
uname-m:	x86_64
uname-r:	6.12.0-0.rc7.58.fc42.x86_64

files:
systemtap.log
systemtap.sum

EOF

diff r-show-testrun-ref.txt r-show-testrun.txt

# pwd > /tmp/xxx
# sleep 10000


exit 0
