#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

export GIT_AUTHOR_DATE="1970-01-01T01:01"

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/automake/a; t-upload-git-push $gitrepo $tag *.log *.trs)

# Run the bunsen pipeline
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db --loglevel=debug

sqlite3 $db "select key,value from testrun_kv where key not like '%authored%' and key not like '%gitdescribe%' order by key" | tee keywords.txt
# or use                                      where key in ("a", "b", "c")

cat <<'EOF' > keywords-expected.txt
omnitest.results|PASS=1
EOF

diff -u keywords-expected.txt keywords.txt 

# Assert summary counts
sqlite3 $db 'select result,sum(number) from automake_testsuite_summary_v group by result order by result' | tee results.txt

cat <<'EOF' > results-expected.txt
PASS|1
EOF

diff -u results-expected.txt results.txt 

# Delete the testrun commit and rerun pipeline
(cd $gitrepo; git tag -d $tag; git gc)
pipeline --git $gitrepo --db $db --loglevel=debug

# Expect emptiness
sqlite3 $db 'select result,sum(number) from automake_testsuite_summary_v group by result order by result' | tee results2.txt

diff -u /dev/null results2.txt

exit 0
