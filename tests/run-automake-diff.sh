#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Import the test files

(cd $abs_srcdir/testdata/automake/a; t-upload-git-push $gitrepo abraca *.log *.trs)
(cd $abs_srcdir/testdata/automake/b; t-upload-git-push $gitrepo dabra  *.log *.trs)

# Run the bunsen pipeline
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db --loglevel=debug

r-diff-testruns --git $gitrepo --db $db abraca dabra --loglevel=debug | tee results.txt
cat <<'EOF' > results-expected.txt

	abraca
		dabra

metadata diffs

omnitest.results
	PASS=1
		FAIL=1

testrun.gitdescribe
	abraca
		dabra

automake diffs

 run-gdb-keiths.sh.trs
	PASS	run-gdb-keiths.sh.trs	run-gdb-keiths.sh.log
		FAIL	run-gdb-keiths.sh.trs	run-gdb-keiths.sh.log
EOF
diff -u results-expected.txt results.txt 

r-diff-testruns --git $gitrepo --db $db --regression abraca dabra --loglevel=debug | tee results.txt
cat <<'EOF' > results-expected.txt

	abraca
		dabra

metadata diffs

omnitest.results
	PASS=1
		FAIL=1

testrun.gitdescribe
	abraca
		dabra
EOF
diff -u results-expected.txt results.txt 


r-diff-testruns --git $gitrepo --db $db --regressions dabra abraca --loglevel=debug | tee results.txt
cat <<'EOF' > results-expected.txt

	dabra
		abraca

metadata diffs

omnitest.results
	FAIL=1
		PASS=1

testrun.gitdescribe
	dabra
		abraca

automake diffs

 run-gdb-keiths.sh.trs
	FAIL	run-gdb-keiths.sh.trs	run-gdb-keiths.sh.log
		PASS	run-gdb-keiths.sh.trs	run-gdb-keiths.sh.log
EOF
diff -u results-expected.txt results.txt 


exit 0
