#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2024 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
#set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/gcc-multilib; t-upload-git-push $gitrepo $tag *.log *.sum)

# Rerun the bunsen pipeline
pipeline --git $gitrepo --db $db --loglevel=debug

sqlite3 $db "select key,value from testrun_kv where key not like '%authored%' and key not like '%gitdescribe%' order by key" | tee keywords.txt
# or use				      where key in ("a", "b", "c")

cat <<'EOF' > keywords-expected.txt
architecture|x86_64
g++_version|/gcc-BUILD/gcc/xg++  version 15.0.0 20240907 (experimental) (GCC)
gcc_version|/gcc-BUILD/gcc/xgcc  version 15.0.0 20240907 (experimental) (GCC)
libgo_version|/home/builder/shared/bb3/worker/gcc-fullest-debian-amd64/gcc-build/./gcc/gccgo version 15.0.0 20240911 (experimental) (GCC)
omnitest.results|PASS=30379 XFAIL=1098 UNSUPPORTED=60 FAIL=3
target_board|unix unix/-m32
EOF

diff -u keywords-expected.txt keywords.txt 


# Assert summary counts
sqlite3 $db 'select result,sum(number) from dejagnu_testsuite_expfile_summary_v group by result order by result' | tee results.txt

cat <<'EOF' > results-expected.txt
FAIL|3
PASS|30379
UNSUPPORTED|60
XFAIL|1098
EOF

diff -u results-expected.txt results.txt 

# Print some details via r-testrun-show
cat <<'EOF' > results-expected.txt
g++.log
g++.sum
gcc.log
gcc.sum
libgo.log
libgo.sum
EOF

r-show-testrun --dejagnu --clusters --git $gitrepo --db $db --template=files $tag | tee results.txt
diff -u results-expected.txt results.txt

cat <<'EOF' > results-expected.txt
g++.sum	g++.dg/analyzer/analyzer.exp [unix]	9227 PASS	329 XFAIL	16 UNSUPPORTED
g++.sum	g++.dg/analyzer/analyzer.exp [unix/-m32]	9212 PASS	329 XFAIL	22 UNSUPPORTED
gcc.sum	gcc.dg/analyzer/analyzer.exp [unix]	5895 PASS	220 XFAIL	5 UNSUPPORTED
gcc.sum	gcc.dg/analyzer/analyzer.exp [unix/-m32]	5656 PASS	220 XFAIL	17 UNSUPPORTED
libgo.sum	libgo.exp [unix]	194 PASS	2 FAIL
libgo.sum	libgo.exp [unix/-m32]	195 PASS	1 FAIL
EOF
r-show-testrun --dejagnu --clusters --git $gitrepo --db $db $tag | fgrep .exp > results.txt
diff -u results-expected.txt results.txt

cat <<'EOF' > results-expected.txt
dejagnu|FAIL|3
dejagnu|PASS|30379
dejagnu|UNSUPPORTED|60
dejagnu|XFAIL|1098
EOF
sqlite3 $db 'select testsuite, result, number from omnitest_summary_v order by result, number;' > results.txt
diff -u results-expected.txt results.txt

# Delete the testrun commit and rerun pipeline
(cd $gitrepo; git tag -d $tag; git gc)
pipeline --git $gitrepo --db $db --loglevel=debug

# Expect emptiness
sqlite3 $db 'select result,sum(number) from dejagnu_testsuite_expfile_summary_v group by result order by result' | tee results2.txt

diff -u /dev/null results2.txt

exit 0
