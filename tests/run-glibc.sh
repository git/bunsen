#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Import the test files
tag=arbitrary
commit1=`(cd $abs_srcdir/testdata/glibc; find . | env GIT_AUTHOR_DATE="1970-01-01T01:01" t-upload-git-push $gitrepo $tag) | awk '{print $1}' `

# Run the bunsen pipeline
pipeline --git $gitrepo --db $db --loglevel=debug

# Expect no dejagnu hits, despite .sum file
cat <<EOF > results-expected.txt
EOF
sqlite3 $db 'select * from dejagnu_testsuite' | tee results.txt
diff results.txt results-expected.txt

# Basic parsing
cat <<EOF > results-expected.txt
|conform.out|conform.test-result|PASS
EOF
sqlite3 $db 'select testdir, outfile, trfile, result from glibc_testcase_v order by testdir, outfile, trfile' | tee results.txt
diff results.txt results-expected.txt


# Summarizing
cat <<EOF > results-expected.txt
|PASS|1
EOF
sqlite3 $db 'select tss.name, tsv.result, tsv.number from glibc_testsuite ts, glibc_testsuite_summary_v tsv, glibc_string tss where tsv.ts = ts.id and tss.id = ts.testdir order by ts.testdir, tsv.result' | tee results.txt
diff results.txt results-expected.txt


cat <<EOF > results-expected.txt
[
  {
    "outfile": "conform.out",
    "result": "PASS",
    "trfile": "conform.test-result"
  }
]
EOF
r-show-testrun --clusters --git $gitrepo --db $db --glibctestdir="" --template=json $tag | jq .glibctestcases | tee results.txt
diff results.txt results-expected.txt

# check that auto_vacuum stuck
sqlite3 $db 'pragma auto_vacuum;'

exit 0
