#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Create a upstream repo
upstream=`pwd`/upstream
mkdir $upstream
git init $upstream

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Generate a bunch of test content.
date=`date +%s`
mkdir testdir
for size in 1 3   100 600 700 8 10 30   900 1000 50 70 90 
do
    # create corresponding upstream commit
    (cd $upstream; touch test$size.source; git add test$size.source; git commit -mtestsource)
    # MAYBE tag it
    if [ $size -lt 100 ]; then
        (cd $upstream; git tag "test$size")
    fi
    (cd testdir;
    echo "$upstream" > .bunsen.source.gitrepo
    echo `cd $upstream; git rev-parse HEAD` > .bunsen.source.gitname
    grep . .bunsen.*)

    date=`expr $date - 86400` # consecutive days before today
    tag="test/time$date/size$size"
    # add a new file
    dd if=/dev/zero of="testdir/file$i.nothing" bs=1024 count=$size
    (cd testdir; find . | env GIT_AUTHOR_DATE="$date 0" t-upload-git-push $gitrepo $tag) # add all prev. and new file here
done
rm -r testdir
(cd $gitrepo; git repack -a -d)

# Run the bunsen pipeline
pipeline --git $gitrepo --db $db --loglevel=debug
clusters1=`sqlite3 $db 'select count(*) from testrun_cluster'`

# Age out a few of the oldest only
t-untag-testruns --loglevel=debug --git $gitrepo --scale-factor 0 --untag-percent 10

# Rerun
pipeline --git $gitrepo --db $db --loglevel=debug 2>&1 | grep 'INFO:testruns.*dead 1'
clusters2=`sqlite3 $db 'select count(*) from testrun_cluster'`

# Don't age any
t-untag-testruns --loglevel=debug --git $gitrepo --untag-percent 50 -n
pipeline --git $gitrepo --db $db --loglevel=debug 2>&1 | grep 'INFO:testruns.*dead 0'
clusters3=`sqlite3 $db 'select count(*) from testrun_cluster'`

# Age out a few of the largest only
t-untag-testruns --loglevel=debug --git $gitrepo --age-factor 0 --untag-percent 20

# Rerun
pipeline --git $gitrepo --db $db --loglevel=debug 2>&1 | grep 'INFO:testruns.*dead 2'
clusters4=`sqlite3 $db 'select count(*) from testrun_cluster'`

# Age out everything else upstream-untagged
sqlite3 $db 'select * from testrun_kv, testrun where tr = id'
t-untag-testruns --loglevel=debug --db $db --git $gitrepo --untag-percent 100 --keep-upstream-tagged
(cd $gitrepo; git gc)

pipeline --git $gitrepo --db $db --loglevel=debug 2>&1
pipeline --git $gitrepo --db $db --loglevel=debug 2>&1 | grep 'INFO:testruns.*new 0, old 7, dead 0'
clusters5=`sqlite3 $db 'select count(*) from testrun_cluster'`

# Age out everything else
t-untag-testruns --loglevel=debug --git $gitrepo --untag-percent 100
(cd $gitrepo; git gc)

# Rerun
pipeline --git $gitrepo --db $db --loglevel=debug 2>&1
pipeline --git $gitrepo --db $db --loglevel=debug 2>&1 | grep 'INFO:testruns.*new 0, old 0, dead 0'
clusters6=`sqlite3 $db 'select count(*) from testrun_cluster'`

# Check cluster sizes
echo "Clusters: $clusters1 $clusters2 $clusters3 $clusters4 $clusters5 $clusters6"
test $clusters1 -gt $clusters2
test $clusters2 -eq $clusters3
test $clusters3 -gt $clusters4
test $clusters4 -gt $clusters5
test $clusters5 -gt $clusters6
test $clusters6 -eq 0

exit 0
