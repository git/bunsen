#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

# Snippets from elfutils test-subr.sh

# Each test runs in its own directory to make sure they can run in parallel.
test_dir="testrun-$$"
mkdir -p "$test_dir"
original_cwd="`pwd`"
cd "$test_dir"

# Tests that trap EXIT (0) themselves should call this explicitly.
exit_cleanup()
{
  cd "$original_cwd"; rm -fr "$test_dir"
}
trap exit_cleanup 0

# A git user.email/user.name config or these environment variables
# are; otherwise git-commit in t-upload-git-push fails.
GIT_AUTHOR_NAME="Bunsen"
GIT_COMMITTER_NAME=$GIT_AUTHOR_NAME
GIT_AUTHOR_EMAIL="bunsen@example.com"
GIT_COMMITTER_EMAIL=$GIT_AUTHOR_EMAIL
export GIT_AUTHOR_NAME GIT_COMMITTER_NAME GIT_AUTHOR_EMAIL GIT_COMMITTER_EMAIL
