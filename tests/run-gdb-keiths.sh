#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo


# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/gdb-keiths; t-upload-git-push $gitrepo $tag gdb.log gdb.sum)

# Rerun the bunsen pipeline
pipeline --git $gitrepo --db $db --loglevel=debug

sqlite3 $db "select key,value from testrun_kv where key not like '%authored%' and key not like '%gitdescribe%' order by key" | tee keywords.txt
# or use                                      where key in ("a", "b", "c")

cat <<'EOF' > keywords-expected.txt
architecture|x86_64
compiler_info|gcc-10-3-1
gdb_version|/home/fedora33/work/gdb/fsf/virgin/linux/gdb/gdb version  13.0.50.20220412-git -nw -nx -iex "set height 0" -iex "set width 0" -data-directory /home/fedora33/work/gdb/fsf/virgin/linux/gdb/testsuite/../data-directory  -iex "set auto-connect-native-target off" -iex "set sysroot"
omnitest.date|2022-04-14
omnitest.results|PASS=3 ERROR=2 FAIL=1 KFAIL=1 KPASS=1 PATH=1 UNRESOLVED=1 UNSUPPORTED=1 UNTESTED=1 XFAIL=1 XPASS=1
target_board|native-gdbserver/-m32
EOF

diff -u keywords-expected.txt keywords.txt 


# Assert summary counts
sqlite3 $db 'select result,sum(number) from dejagnu_testsuite_expfile_summary_v group by result order by result' | tee results.txt

cat <<'EOF' > results-expected.txt
ERROR|2
FAIL|1
KFAIL|1
KPASS|1
PASS|3
PATH|1
UNRESOLVED|1
UNSUPPORTED|1
UNTESTED|1
XFAIL|1
XPASS|1
EOF

diff -u results-expected.txt results.txt 

# Print some details via r-testrun-show
cat <<'EOF' > results-expected.txt
gdb.log
gdb.sum
EOF

r-show-testrun --dejagnu --clusters --git $gitrepo --db $db --template=files $tag | tee results.txt
diff -u results-expected.txt results.txt

cat <<'EOF' > results-expected.txt
gdb.sum	gdb.gdb/test_outcomes.exp [native-gdbserver/-m32]	3 PASS	1 FAIL	1 XPASS	1 XFAIL	1 KFAIL	1 KPASS	2 ERROR	1 UNTESTED	1 UNRESOLVED	1 UNSUPPORTED	1 PATH
EOF
r-show-testrun --dejagnu --clusters --git $gitrepo --db $db $tag | fgrep .exp > results.txt
diff -u results-expected.txt results.txt

cat <<'EOF' > results-expected.txt
dejagnu|ERROR|2
dejagnu|FAIL|1
dejagnu|KFAIL|1
dejagnu|KPASS|1
dejagnu|PASS|3
dejagnu|PATH|1
dejagnu|UNRESOLVED|1
dejagnu|UNSUPPORTED|1
dejagnu|UNTESTED|1
dejagnu|XFAIL|1
dejagnu|XPASS|1
EOF
sqlite3 $db 'select testsuite, result, number from omnitest_summary_v order by result, number;' > results.txt
diff -u results-expected.txt results.txt

# Delete the testrun commit and rerun pipeline
(cd $gitrepo; git tag -d $tag; git gc)
pipeline --git $gitrepo --db $db --loglevel=debug

# Expect emptiness
sqlite3 $db 'select result,sum(number) from dejagnu_testsuite_expfile_summary_v group by result order by result' | tee results2.txt

diff -u /dev/null results2.txt

exit 0
