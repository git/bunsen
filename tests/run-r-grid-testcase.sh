#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

trids=""
# Import test logs
pushd $abs_srcdir/testdata/r-grid-testcase
    for i in el*; do
        d=$(echo $i | awk -F- '{print $3"-"$4"-"$5}')
        output=$(find $i | env GIT_AUTHOR_DATE="${d}T01:01" t-upload-git-push $gitrepo $i)
        trids="$trids $(echo "$output" | awk '{print $1}')"
    done
popd

# Run the bunsen pipeline
pipeline --git $gitrepo --db $db

trid1=$(echo $trids | awk '{print $1}')
r-grid-testcase --trid $trid1 --toplevel --template json --loglevel debug \
                --h_limit 2 --v_limit 2 --vertical architecture \
                --horizontal=omnitest.date > raw_output.txt

jq -r ".v_keywords[]" raw_output.txt | tee results.txt
cat > expected-results.txt <<EOF1
aarch64
powerpc64le
EOF1
diff -u results.txt expected-results.txt

jq -r ".h_keywords[]" raw_output.txt | tee results.txt
cat > expected-results.txt <<EOF2
2025-01-17
2025-01-15
EOF2
diff -u results.txt expected-results.txt

jq -r '.results[] | "\(.horizontal) \(.vertical) \(.result) \(.number)"' raw_output.txt | sort | tee results.txt
cat > expected-results.txt <<EOF3
2025-01-15 powerpc64le ERROR 1
2025-01-15 powerpc64le FAIL 1921
2025-01-15 powerpc64le KFAIL 304
2025-01-15 powerpc64le KPASS 23
2025-01-15 powerpc64le PASS 8936
2025-01-15 powerpc64le UNSUPPORTED 25
2025-01-15 powerpc64le UNTESTED 843
2025-01-15 powerpc64le WARNING 8
2025-01-15 powerpc64le XFAIL 712
2025-01-15 powerpc64le XPASS 11
2025-01-17 aarch64 FAIL 1846
2025-01-17 aarch64 KFAIL 284
2025-01-17 aarch64 KPASS 8
2025-01-17 aarch64 PASS 9187
2025-01-17 aarch64 UNSUPPORTED 37
2025-01-17 aarch64 UNTESTED 739
2025-01-17 aarch64 WARNING 2
2025-01-17 aarch64 XFAIL 708
2025-01-17 aarch64 XPASS 10
2025-01-17 powerpc64le FAIL 1831
2025-01-17 powerpc64le KFAIL 301
2025-01-17 powerpc64le KPASS 25
2025-01-17 powerpc64le PASS 8725
2025-01-17 powerpc64le UNSUPPORTED 25
2025-01-17 powerpc64le UNTESTED 837
2025-01-17 powerpc64le WARNING 2
2025-01-17 powerpc64le XFAIL 707
2025-01-17 powerpc64le XPASS 10
EOF3
diff -u results.txt expected-results.txt

r-grid-testcase --trid $trid1 --toplevel --template json --loglevel debug \
                --h_limit 4 --v_limit 3 --vertical architecture \
                --opt_keyword architecture '*s390x*' \
                --horizontal=omnitest.date > raw_output.txt

jq -r ".h_keywords[]" raw_output.txt | sort | tee results.txt
cat > expected-results.txt <<EOF4
2025-01-10
2025-01-13
2025-01-15
2025-01-17
EOF4
diff -u expected-results.txt results.txt

jq -r ".v_keywords[]" raw_output.txt | sort | tee results.txt
cat > expected-results.txt <<EOF5
aarch64
powerpc64le
s390x
EOF5
diff -u expected-results.txt results.txt

jq -r '.results[] | "\(.horizontal) \(.vertical) \(.result) \(.number)"' raw_output.txt | sort | tee results.txt
cat > expected-results.txt <<EOF6
2025-01-10 s390x FAIL 1561
2025-01-10 s390x KFAIL 111
2025-01-10 s390x KPASS 5
2025-01-10 s390x PASS 8082
2025-01-10 s390x UNSUPPORTED 31
2025-01-10 s390x UNTESTED 846
2025-01-10 s390x WARNING 2
2025-01-10 s390x XFAIL 356
2025-01-10 s390x XPASS 5
2025-01-13 s390x FAIL 1793
2025-01-13 s390x KFAIL 110
2025-01-13 s390x KPASS 5
2025-01-13 s390x PASS 7753
2025-01-13 s390x UNSUPPORTED 31
2025-01-13 s390x UNTESTED 835
2025-01-13 s390x WARNING 2
2025-01-13 s390x XFAIL 353
2025-01-13 s390x XPASS 5
EOF6
#diff -u results.txt expected-results.txt


trid3=$(echo $trids | awk '{print $3}')
r-grid-testcase --trid $trid3 --template json --loglevel error \
                --test 'systemtap.base/global_vars.exp [unix]' \
                --subtest 'global_vars startup' --h_limit 3 --v_limit 3 \
                --vertical architecture --horizontal omnitest.date > raw_output.txt

jq -r ".h_keywords[]" raw_output.txt | sort | tee results.txt
cat > expected-results.txt <<EOF7
2025-01-10
2025-01-13
2025-01-15
EOF7
diff -u results.txt expected-results.txt

jq -r ".v_keywords[]" raw_output.txt | sort | tee results.txt
cat > expected-results.txt <<EOF8
powerpc64le
s390x
x86_64
EOF8
diff -u results.txt expected-results.txt

jq -r '.results[] | "\(.horizontal) \(.vertical) \(.result)"' raw_output.txt | sort | tee results.txt
cat > expected-results.txt <<EOF9
2025-01-10 s390x PASS
2025-01-10 x86_64 PASS
2025-01-10 x86_64 PASS
2025-01-10 x86_64 PASS
2025-01-10 x86_64 PASS
2025-01-13 s390x PASS
2025-01-15 powerpc64le PASS
2025-01-15 powerpc64le PASS
EOF9
diff -u results.txt expected-results.txt

exit 0
