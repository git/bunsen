#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

# Make common.py available when not installed to --prefix
export PYTHONPATH=$abs_srcdir/../lib:$PYTHONPATH

# Use the build tree instance of r-httpd-browse
PATH=$abs_builddir/../bin:$PATH

set -x
set -e

# Prepare cleanup routine
cleanup()
{
    file=$1
    pid=$2
    rm -rf "$file" ||:
    kill -hup "$pid" ||:
}

# Check for pre-requisities
for c in lynx nc wget; do
   command -v $c || exit 77
done

# Identify a free TCP port.
myhost=127.0.0.1
myport=8005
while true; do
    nc -z $myhost $myport >/dev/null || break
    if test $myport -ge 65535; then
        echo "ERROR: No free TCP port found. Terminating."
        exit 1
    fi
    myport=$((myport+1))
done
echo "Going to use port $myport for testing"

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/gdb-keiths; t-upload-git-push $gitrepo $tag gdb.log gdb.sum)

# Rerun the bunsen pipeline
pipeline --git $gitrepo --db $db --loglevel=debug

# Run the http server
r-httpd-browse --git $gitrepo --db $db --bind $myhost --port $myport --max-classify 0 --pkgdatadir=$abs_srcdir/../data &
mypid=$!
dumpfile=$(mktemp)

# Some startup time for the server
sleep 0.5

# Set up the cleanup routine
trap 'cleanup $dumpfile $mypid' 0 1 2 3 5 15

# Identify commit hash
commit=$(r-find-testruns --template=json --git $gitrepo --db $db |\
         jq '.testruns[0].testrun' | tr -d '"')

# TEST 1:  Overview
lynx -width=300 -dump "http://$myhost:$myport/testruns/?limit=1000" | tee $dumpfile
for i in "UNTESTED=\d+" "XFAIL=\d+" "KFAIL=\d+" "UNSUPPORTED=\d+" "XPASS=\d+" \
         "KPASS=\d+" "filter testrun commitishes"; do
    grep -P "$i" $dumpfile
done

# TEST 2:  Summary
lynx -width=300 -dump "http://$myhost:$myport/testrun/$commit" | tee $dumpfile
for i in "architecture" "compiler_info" "gdb_version" "omnitest.results" \
         "target_board" "testrun.authored.day" "testrun.authored.time" \
         "testrun.gitdescribe" "testrun $commit" "focusing on summary, reset to" \
         "testsuite summary"; do
    grep -P "$i" $dumpfile
done

# TEST 3:  Clusters
lynx -width=300 -dump "http://$myhost:$myport/testrun/$commit?focus=clusters" | tee $dumpfile
for i in "architecture" "compiler_info" "gdb_version" "omnitest.results" \
         "target_board" "testrun.authored.day" "testrun.authored.time" \
         "testrun.gitdescribe" "testrun $commit" "focusing on clusters, reset to"; do
    grep -P "$i" $dumpfile
done

# TEST 4:  Filelist
lynx -width=300 -dump "http://$myhost:$myport/testrun/$commit?focus=filelist" | tee $dumpfile
for i in "browse" "raw" "gdb.log" "gdb.sum" "testrun $commit" \
         "focusing on filelist, reset to"; do
    grep -P "$i" $dumpfile
done

# TEST 5:  Dejagnu
lynx -width=300 -dump "http://$myhost:$myport/testrun/$commit?focus=dejagnu" | tee $dumpfile
for i in "gdb.gdb/test_outcomes.exp" "testrun $commit" \
         "focusing on dejagnu, reset to"; do
    grep -P "$i" $dumpfile
done

# TEST 6:  r-grid-testcase
lynx -width=300 -dump "http://$myhost:$myport/r_grid_testcase/?trid=$commit&toplevel=True" |\
     tee $dumpfile
for i in "Vertical Proximity Limit:" "Horizontal Proximity Limit:" \
         "compare selected testruns"; do
    grep -P "$i" $dumpfile
done

# TEST 7:  Check for broken links with wget --spider
#
# Note that there are two significantly different versions of wget in fedora:
# wget1 (e.g. wget-1.21.3-1.fc36) and wget2 (e.g. wget2-2.1.0-11.fc40) with
# different behavior and different command line options.  There also is bug 
# 2317774 related to robots.txt.
wget --spider --recursive --verbose "http://$myhost:$myport/testruns/?limit=1000"

# Import an autotest log
tag=arbitrary-autotest-$$
(cd $abs_srcdir/testdata/autotest/debugedit; t-upload-git-push $gitrepo $tag *.log)

# Run the bunsen pipeline
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db --loglevel=debug

# TEST 8:  autotest / debugedit
lynx -width=300 -dump "http://$myhost:$myport/testruns/?limit=1000" | tee $dumpfile
for i in "arbitrary-autotest-\d+" "PASS=25" "FAIL=2"; do
    grep -P "$i" $dumpfile
done

exit 0

