#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

export PATH=$abs_srcdir/../bin:$PATH
export GIT_AUTHOR_DATE="1970-01-01T01:01"

# Make common.py available when not installed to --prefix
export PYTHONPATH=$abs_srcdir/../lib:$PYTHONPATH

# Use the build tree instance of r-find-testruns-with-commit
PATH=$abs_builddir/../bin:$PATH

set -x
set -e

db="`pwd`/bunsen.sqlite3"

# run the pipeline against an empty repo just to get a database file having
# needed tables ready in it.
mkdir emptyrepo
pushd emptyrepo
git init .

pipeline
mv bunsen.sqlite3 $db
popd
rm -rf emptyrepo

# populate the run-r-find-testruns-with-commit's cache with some minimal data
# needed for testing, --cachedir `pwd` will be used later on to point to this
# fake cache.
tarball="$abs_srcdir/testdata/r-find-testruns-with-commit/refrepo-ada5686e1603dffa497a043c490138a3.tar.xz"
md5sum $tarball | grep -F 1408c57876de3aa41ac6edc6088d79a9
tar xJf $tarball

# populate the database file ($db) with some minimal data needed for testing
echo """
insert into testrun (gitcommit) values ('947309a32f894f189430c9241840bad83f869304');
insert into testrun (gitcommit) values ('69b362a79f8b6bda5202decaad918dba9422d120');
insert into testrun (gitcommit) values ('8b87bd584dcadb3713eaf6d0c9b540b4f54124bf');
insert into testrun (gitcommit) values ('3a92ffe673c1621309a0b60892114495b651c9de');
insert into testrun (gitcommit) values ('069def0ae9184911ad1d7810f9febd620d0e4534');
insert into testrun (gitcommit) values ('8ee93b7fe34ff7fdb8296726811bdd7bb530f97f');
insert into testrun (gitcommit) values ('6c302e4a67b8d5db527f464f33eb2f83a539cd3d');
insert into testrun (gitcommit) values ('59d16b9e2c5d37f68dd052653a7f23d783e6f114');
insert into testrun (gitcommit) values ('c955d1873d0f9a18ea246061ac4cce6a7c239fb1');
-- these commits are in branch main
insert into testrun_kv (tr, key, value) values (1, 'source.gitname', '4f2fd68c9df089e4dccf0487cb3a51dcc7312470');
insert into testrun_kv (tr, key, value) values (2, 'source.gitname', 'a342037f0f6e27886c6750474b7384c2c915b012');
insert into testrun_kv (tr, key, value) values (3, 'source.gitname', '6a1b561805ea219f980cf369d79a3ca97678d0ab');
insert into testrun_kv (tr, key, value) values (4, 'source.gitname', '8845fdaa7c1385e98d673f84f60d4dbfbc025c61');
insert into testrun_kv (tr, key, value) values (5, 'source.gitname', '7e5f7bfd418552aaafb7198dee9a724f52a38578');
-- now fork and create mybranch.  following commits belong to mybranch only:
insert into testrun_kv (tr, key, value) values (6, 'source.gitname', 'b5b8cad575904683eaa550c68cf817c03b932b85');
insert into testrun_kv (tr, key, value) values (7, 'source.gitname', '023d5d1018eca4655636e3d86a55c4f0c8d08a3c');
-- following commits only belong to branch main (are not on branch mybranch):
insert into testrun_kv (tr, key, value) values (8, 'source.gitname', '51e0f0e814249c4a92a90082f752a9216b6ec019');
insert into testrun_kv (tr, key, value) values (9, 'source.gitname', '89eb915b32bbebfa58960ba996cbd418a040d82a');
""" | sqlite3 $db

# the tool will always refer to --giturl https://github.com/rh-mcermak/test.git
# this url will never be fetched because of --skiprefresh.  The url actuallu only
# serves as a pointer to the cache (indirectly, via md5sum)
TOOL="r-find-testruns-with-commit --loglevel=debug --db=$db --cachedir `pwd` --giturl https://github.com/rh-mcermak/test.git --skiprefresh"

echo "# Test 01"
$TOOL --maxdelta 0 7e5f7bfd418552aaafb7198dee9a724f52a38578 > std.out 2> err.out
cat err.out
cat > std.out.ref <<EOF1
    0 7e5f7bfd418552aaafb7198dee9a724f52a38578 069def0ae9184911ad1d7810f9febd620d0e4534 main,origin/HEAD,origin/main,origin/mybranch
EOF1
diff -u std.out std.out.ref

echo "# Test 01a"
$TOOL --describe --maxdelta 0 7e5f7bfd4 > std.out 2> err.out
cat err.out
grep '^    0 7e5f[0-9a-f]* 069def0ae9184911ad1d7810f9febd620d0e4534 main,origin/HEAD,origin/main,origin/mybranch$' < std.out

echo "# Test 02"
$TOOL --maxdelta 4 6a1b561805ea219f980cf369d79a3ca97678d0ab 2> err.out | head -5 > std.out
cat err.out
cat > std.out.ref <<EOF2
    0 6a1b561805ea219f980cf369d79a3ca97678d0ab 8b87bd584dcadb3713eaf6d0c9b540b4f54124bf main,origin/HEAD,origin/main,origin/mybranch
    1 a342037f0f6e27886c6750474b7384c2c915b012 69b362a79f8b6bda5202decaad918dba9422d120 main,origin/HEAD,origin/main,origin/mybranch
   -1 8845fdaa7c1385e98d673f84f60d4dbfbc025c61 3a92ffe673c1621309a0b60892114495b651c9de main,origin/HEAD,origin/main,origin/mybranch
    2 4f2fd68c9df089e4dccf0487cb3a51dcc7312470 947309a32f894f189430c9241840bad83f869304 main,origin/HEAD,origin/main,origin/mybranch
   -2 7e5f7bfd418552aaafb7198dee9a724f52a38578 069def0ae9184911ad1d7810f9febd620d0e4534 main,origin/HEAD,origin/main,origin/mybranch
EOF2
diff -u std.out std.out.ref

echo "# Test 03"
$TOOL --maxdelta 4 6a1b561805ea219f980cf369d79a3ca97678d0ab --older 2> err.out | head -3 > std.out
cat err.out
cat > std.out.ref <<EOF3
    0 6a1b561805ea219f980cf369d79a3ca97678d0ab 8b87bd584dcadb3713eaf6d0c9b540b4f54124bf main,origin/HEAD,origin/main,origin/mybranch
   -1 8845fdaa7c1385e98d673f84f60d4dbfbc025c61 3a92ffe673c1621309a0b60892114495b651c9de main,origin/HEAD,origin/main,origin/mybranch
   -2 7e5f7bfd418552aaafb7198dee9a724f52a38578 069def0ae9184911ad1d7810f9febd620d0e4534 main,origin/HEAD,origin/main,origin/mybranch
EOF3
diff -u std.out std.out.ref

echo "# Test 04"
$TOOL --maxdelta 4 6a1b561805ea219f980cf369d79a3ca97678d0ab --newer 2> err.out | head -3 > std.out
cat err.out
cat > std.out.ref <<EOF4
    0 6a1b561805ea219f980cf369d79a3ca97678d0ab 8b87bd584dcadb3713eaf6d0c9b540b4f54124bf main,origin/HEAD,origin/main,origin/mybranch
    1 a342037f0f6e27886c6750474b7384c2c915b012 69b362a79f8b6bda5202decaad918dba9422d120 main,origin/HEAD,origin/main,origin/mybranch
    2 4f2fd68c9df089e4dccf0487cb3a51dcc7312470 947309a32f894f189430c9241840bad83f869304 main,origin/HEAD,origin/main,origin/mybranch
EOF4
diff -u std.out std.out.ref

echo "# Test 06"
echo $TOOL --maxdelta 4 6a1b561805ea219f980cf369d79a3ca97678d0ab
$TOOL --maxdelta 4  6a1b561805ea219f980cf369d79a3ca97678d0ab --template json | tee result.json
cat result.json | 
    jq ".commits[4].commit" | tr -d '"' |
    grep '^7e5f7bfd418552aaafb7198dee9a724f52a38578$'
cat result.json |
    jq ".commits[4].testrun" | tr -d '"' |
    grep '^069def0ae9184911ad1d7810f9febd620d0e4534$'
cat result.json |
    jq ".commits[4].branches" | tr -d '"' |
    grep '^main,origin/HEAD,origin/main,origin/mybranch$'
cat result.json |
    jq ".commits[4].delta" | tr -d '"' |
    grep '^-2$'

echo "# Test 09"
$TOOL --exact 6a1b561805ea219f980cf369d79a3ca97678d0ab --newer 2> err.out > std.out
cat err.out
cat > std.out.ref <<EOF6
    0 6a1b561805ea219f980cf369d79a3ca97678d0ab 8b87bd584dcadb3713eaf6d0c9b540b4f54124bf main,origin/HEAD,origin/main,origin/mybranch
EOF6
diff -u std.out std.out.ref

echo "# Test 10"
$TOOL --exact --maxdelta=1 6a1b561805ea219f980cf369d79a3ca97678d0ab --newer 2> err.out > std.out ||:
grep -F -- '--maxdelta: not allowed with argument --exact' err.out

exit 0
