#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

export PATH=$abs_srcdir/../bin:$PATH
export GIT_AUTHOR_DATE="1970-01-01T01:01"

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/autotest/debugedit; t-upload-git-push $gitrepo $tag testsuite.log)

# Rerun the bunsen pipeline
pipeline --git $gitrepo --db $db


# testcase 1.1

sqlite3 $db "select result, number from autotest_testsuite_summary_v order by number" | tee summary.txt

cat <<'EOF' > expected.summary.txt
FAIL|2
PASS|25
EOF

diff -u expected.summary.txt summary.txt

# testcase 1.2

sqlite3 $db "select tcname, result from autotest_testcase_v order by tcnameid" | tee tclist.txt

grep -C1 -F FAIL tclist.txt > tclist.part.txt

cat <<'EOF' > expected.tclist.part.txt
debugedit --list-file DWARF5|PASS
debugedit .debug_types partial|FAIL
debugedit .debug_line partial DWARF5|FAIL
EOF

diff -u expected.tclist.part.txt tclist.part.txt

# testcase 1.3

commitish=$(r-find-testruns --git $gitrepo --db $db --template json | jq '.testruns[0].testrun' | tr -d '"')

r-show-testrun --git $gitrepo --db $db --autotest $commitish | tee r-show-testrun.txt

grep -C1 -F FAIL r-show-testrun.txt | tee r-show-testrun.part.txt

# NB: Following check is sensitive to whitespace (Tabs vs. spaces)
cat <<'EOF' > expected.r-show-testrun.part.txt
metadata:
omnitest.results:	PASS=25 FAIL=2
testrun.authored.day:	1970-01-01
--
27. debugedit --list-file DWARF5 PASS
29. debugedit .debug_types partial FAIL
30. debugedit .debug_line partial DWARF5 FAIL

--
testsuite summary:
FAIL	2
PASS	25
EOF

diff -u expected.r-show-testrun.part.txt r-show-testrun.part.txt

# Re-create a testrun repo
rm -rf $gitrepo
mkdir $gitrepo
git init $gitrepo

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/autotest/autoconf; t-upload-git-push $gitrepo $tag testsuite.log)

# Rerun the bunsen pipeline
rm -f $db
pipeline --git $gitrepo --db $db


# testcase 2.1

sqlite3 $db "select result, number from autotest_testsuite_summary_v order by number" | tee summary2.txt

cat <<'EOF' > expected.summary2.txt
FAIL|2
SKIP|6
PASS|572
EOF

diff -u expected.summary2.txt summary2.txt


# testcase 2.2

sqlite3 $db "select tcname, result from autotest_testcase_v order by tcnameid" | tee tclist2.txt

grep -C1 -F FAIL tclist2.txt > tclist.part2.txt

cat <<'EOF' > expected.tclist.part2.txt
Missing AT_CLEANUP|PASS
AT_FAIL_IF without AT_SETUP|PASS
AT_SKIP_IF without AT_SETUP|PASS
--
AT_DATA without AT_SETUP|PASS
AT_XFAIL_IF without AT_SETUP|PASS
AT_KEYWORDS without AT_SETUP|PASS
--
Hard fail|PASS
AT_FAIL_IF|PASS
AT_SKIP_IF|PASS
--
autoscan|PASS
autom4te cache|FAIL
autoconf: the empty token|FAIL
EOF

diff -u expected.tclist.part2.txt tclist.part2.txt

#testcase 2.3

commitish=$(r-find-testruns --git $gitrepo --db $db --template json | jq '.testruns[0].testrun' | tr -d '"')

r-show-testrun --git $gitrepo --db $db --autotest $commitish | tee r-show-testrun2.txt

grep -C1 -F FAIL r-show-testrun2.txt > r-show-testrun.part2.txt

# NB: Following check is sensitive to whitespace (Tabs vs. spaces)
cat <<'EOF' > expected.r-show-testrun.part2.txt
metadata:
omnitest.results:	PASS=572 SKIP=6 FAIL=2
testrun.authored.day:	1970-01-01
--
129. Missing AT_CLEANUP PASS
130. AT_FAIL_IF without AT_SETUP PASS
131. AT_SKIP_IF without AT_SETUP PASS
--
133. AT_DATA without AT_SETUP PASS
134. AT_XFAIL_IF without AT_SETUP PASS
135. AT_KEYWORDS without AT_SETUP PASS
--
147. Hard fail PASS
148. AT_FAIL_IF PASS
149. AT_SKIP_IF PASS
--
581. autoscan PASS
583. autom4te cache FAIL
584. autoconf: the empty token FAIL

--
testsuite summary:
FAIL	2
PASS	572
EOF

diff -u expected.r-show-testrun.part2.txt r-show-testrun.part2.txt

# Re-create a testrun repo
rm -rf $gitrepo
mkdir $gitrepo
git init $gitrepo

# Import the test files
tag=arbitrary-$$
(cd $abs_srcdir/testdata/autotest/debugedit2; t-upload-git-push $gitrepo $tag testsuite.log)

# Rerun the bunsen pipeline
rm -f $db
pipeline --git $gitrepo --db $db


#testcase 3.1

commitish=$(r-find-testruns --git $gitrepo --db $db --template json | jq '.testruns[0].testrun' | tr -d '"')

for atswitches in '--autotest' '--autotest --autotestlog testsuite.log' '--autotestlog testsuite.log'; do

    r-show-testrun --git $gitrepo --db $db $atswitches $commitish | tee r-show-testrun3.txt

    grep -C1 -F SKIP r-show-testrun3.txt > r-show-testrun3-out.txt

    # NB: Following check is sensitive to whitespace (Tabs vs. spaces)
    cat <<'EOF' > r-show-testrun3.ref.txt
metadata:
omnitest.results:	PASS=27 SKIP=3
testrun.authored.day:	1970-01-01
--
19. debugedit .debug_line objects DWARF4 PASS
21. debugedit .debug_line objects DWARF5 SKIP
22. debugedit .debug_line partial DWARF4 PASS
23. debugedit .debug_line partial DWARF5 SKIP
24. debugedit .debug_line exe DWARF4 PASS
25. debugedit .debug_line exe DWARF5 SKIP
26. debugedit .debug_macro objects PASS
--
PASS	27
SKIP	3

EOF

    diff -u r-show-testrun3.ref.txt r-show-testrun3-out.txt

done

#testcase 3.2

r-show-testrun --git $gitrepo --db $db --autotestlog=SOMENONSENSE $commitish | tee r-show-testrun4-out.txt

cat <<EOF > r-show-testrun4.ref.txt
testrun: $commitish

metadata:
omnitest.results:	PASS=27 SKIP=3
testrun.authored.day:	1970-01-01
testrun.authored.time:	1970-01-01 01:01
testrun.gitdescribe:	$tag

testsuite summary:
PASS	27
SKIP	3


files:
testsuite.log

EOF

diff -u r-show-testrun4.ref.txt r-show-testrun4-out.txt

exit 0
