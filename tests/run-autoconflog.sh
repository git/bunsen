#! /bin/sh

# SPDX-License-Identifier: LGPL-3.0-or-later
# Copyright 2022 Red Hat, Inc.

. $abs_srcdir/test-subr.sh

PATH=$abs_srcdir/../bin:$PATH

set -x
set -e

# Create a testrun repo
gitrepo=`pwd`/testruns
mkdir $gitrepo
git init $gitrepo

# Run the bunsen pipeline one time
db=`pwd`/bunsen.sqlite3
pipeline --git $gitrepo --db $db

# Import the test files
tag=arbitrary
commit1=`(cd $abs_srcdir/testdata/autoconflog; find . | env GIT_AUTHOR_DATE="1970-01-01T01:01" t-upload-git-push $gitrepo $tag) | awk '{print $1}' `

# Run the bunsen pipeline
pipeline --git $gitrepo --db $db --loglevel=debug

# Basic parsing
cat <<'EOF' > results-expected.txt
gprofng/config.log|build system type|x86_64-pc-linux-gnu|41
sim/config.log|build system type|x86_64-pc-linux-gnu|41
gprofng/config.log|host system type|x86_64-pc-linux-gnu|43
sim/config.log|host system type|x86_64-pc-linux-gnu|43
gprofng/config.log|target system type|x86_64-pc-linux-gnu|45
sim/config.log|target system type|x86_64-pc-linux-gnu|45
gprofng/config.log|for a BSD-compatible install|/usr/bin/install -c|47
sim/config.log|for x86_64-pc-linux-gnu-gcc|gcc|47
gprofng/config.log|whether build environment is sane|yes|49
gprofng/config.log|for a thread-safe mkdir -p|/usr/bin/mkdir -p|51
gprofng/config.log|for gawk|gawk|53
gprofng/config.log|whether make sets $(MAKE)|yes|55
gprofng/config.log|whether make supports nested variables|yes|57
gprofng/config.log|whether to enable maintainer-specific portions of Makefiles|no|59
gprofng/config.log|for style of include used by make|GNU|61
gprofng/config.log|for x86_64-pc-linux-gnu-gcc|gcc|63
sim/config.log|whether the C compiler works|yes|82
sim/config.log|for C compiler default output file name|a.out|84
sim/config.log|whether we are cross compiling|no|94
gprofng/config.log|whether the C compiler works|yes|98
sim/config.log|for suffix of object files|o|98
gprofng/config.log|for C compiler default output file name|a.out|100
sim/config.log|whether we are using the GNU C compiler|yes|102
sim/config.log|whether gcc accepts -g|yes|106
gprofng/config.log|whether we are cross compiling|no|110
sim/config.log|for gcc option to accept ISO C89|none needed|110
gprofng/config.log|for suffix of object files|o|114
sim/config.log|whether gcc understands -c and -o together|yes|116
gprofng/config.log|whether we are using the GNU C compiler|yes|118
gprofng/config.log|whether gcc accepts -g|yes|122
EOF
sqlite3 $db 'select filename,test,result,cursor from autoconflog_testcase_v order by cursor,filename limit 30' | tee results.txt
diff results-expected.txt results.txt


cat <<'EOF' > results-expected.txt
{
  "gprofng/config.log": 144,
  "sim/config.log": 384
}
EOF
r-show-testrun --clusters --autoconf --db $db --git $gitrepo $tag --template=json | jq .autoconflogtestsuites | tee results.txt
diff results-expected.txt results.txt 

cat <<'EOF' > results-expected.txt
{
  "cursor": 51,
  "filename": "gprofng/config.log",
  "result": "/usr/bin/mkdir -p",
  "test": "for a thread-safe mkdir -p"
}
EOF
r-show-testrun --clusters --autoconf --db $db --git $gitrepo $tag --template=json --autoconflog='gprofng/config.log' | jq '.autoconflogtestcases[5]' | tee results.txt
diff results-expected.txt results.txt

exit 0
